//scansiona tot numeri e li moltiplica attraverso un array
#include <stdio.h>
#define D 5

int main(void)
{
    int v[D], i, s = 1;

    printf("Inserire 5 numeri da moltiplicare:\n");
    for (i = 0; i < D; i++)
        scanf("%d", &v[i]);
    for (i = 0; i < D; i++)
        s = s * v[i];
    for (i = 0; i < D; i++)
        if (i == D-1)
            printf("%d ", v[i]);
        else
            printf("%d x ", v[i]);

    printf("= %d\n", s);
}