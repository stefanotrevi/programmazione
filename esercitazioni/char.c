//distingue i char in base al loro tipo (minuscolo, maiuscolo, numero, carattere speciale)
#include <stdio.h>

int main(void)
{
    char A;

    printf("Inserire un carattere:\n");
    scanf("%c", &A);

    if (A <= 122 && A >= 97)
        printf("minuscola\n");
    else if (A <= 90 && A >= 65)
        printf("maiuscola\n");
    else if (A <= 57 && A >= 48)
        printf("numero\n");
    else
        printf("carattere speciale\n");
}