//esercizio sugli assegnamenti e gli array (massimo)
#include <stdio.h>
#define DIM 100

void lettura(int a[], int dl);
void ordinamento(int v[], int dl);
int findMax(int v[], int dl);

int main(void)
{
    int dl, v[DIM], max;

    printf("Inserire numero di valori:\n");
    scanf("%d", &dl);
    max = findMax(v, dl);
    printf("Il valore massimo è: %d\n", max);

    return 0;
}

void lettura(int a[], int dl)
{
    int i;
    printf("Inserire i valori:\n");
    for (i = 0; i < dl; i++)
        scanf("%d", &a[i]);
}

void ordinamento(int v[], int dl)
{
    int i, flag = 1, temp;

    lettura(v, dl);

    for (i = 0; i < dl - 1; i++)
    {
        if (flag)
        {
            i = 0;
            flag = 0;
        }
        if (v[i + 1] < v[i])
        {
            temp = v[i];
            v[i] = v[i + 1];
            v[i + 1] = temp;
            flag = 1;
            i = 0;
        }
    }
}

int findMax(int v[], int dl)
{
    ordinamento(v, dl);
    return v[dl - 1];
}