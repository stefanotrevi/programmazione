//stampa operazioni fra array
#include <stdio.h>
#define D 10
int main(void)
{
    int v[D], i;
    for (i = 0; i < D; i++)
    {
        v[i] = (i + 1) * (i + 1);
        printf("%d\n", v[i]);
    }
    return 0;
}