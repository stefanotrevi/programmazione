//ATTENZIONE: tenere lontano dalla portata dei bambini
#include <stdio.h>

int main(void)
{
    int M[3][3]; //= {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};
    int i, j;

    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
            M[i][j] = i == j ? 1 : 0;

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            printf("%d ", M[i][j]);
        printf("\n");
    }
    return 0;
}