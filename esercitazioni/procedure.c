//esegue la divisione di due numeri tramite una procedura
#include <stdio.h>

void divisione();

int main(void)
{
    int dv, ds, q, r;

    printf("Inserire dividendo e divisore:\n");
    scanf("%d%d", &dv, &ds);

    divisione(dv, ds, &q, &r);
    printf("Quoziente = %d, Resto = %d\n", q, r);
    return 0;
}

void divisione(int dv, int ds, int *q, int *r)
{
    *q = dv / ds;
    *r = dv % ds;
}