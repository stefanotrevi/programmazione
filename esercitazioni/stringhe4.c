//esercizio su alcune proprietà interessanti per gli array/stringhe
#include <stdio.h>

int lunghezza(char a[5]);

int main(void)
{
    char s[5];
    scanf("%s", s);
    printf("%d\n", lunghezza(s));
    return 0;
}

int lunghezza(char a[5])
{
    int lung = 0;
    while (a[lung] != '\0')
        lung++;
    return lung;
}