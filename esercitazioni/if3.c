//scansiona due numeri e stampa il maggiore
#include <stdio.h>

int main(void)
{
    int a;
    int b;
    
    printf("Inserire i numeri da confrontare: ");
    scanf("%d%d", &a, &b);
    if (a > b)
        printf("Il primo numero (%d) è maggiore.\n", a);
    else if (a == b)
        printf("I due numeri sono uguali.\n");
    else
        printf("Il secondo numero (%d) è più grande.\n", b);
}