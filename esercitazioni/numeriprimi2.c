//scansiona un numero e stampa tutti i numeri primi minori di esso
#include <stdio.h>

int main(void)
{

    int a, b, i, j, f = 0;

    printf("Inserire numero:\n");
    scanf("%d", &a);

    for (i = 2; i <= a; i++)
    {
        b = i;
        j = 2;
        while (j <= b)
        {
            if (j == b)
            {
                printf("%d\n", b);
                b--;
            }
            if (b % j != 0)
                j++;
            else
                break;

        }
    }
}