//scansiona base ed argomento e stampa il logaritmo dell'argomento nella base.
#include <stdio.h>

int main(void)
{

    int arg, base, esp = 1, p;

    printf("Inserire argomento e base del logaritmo: ");
    scanf("%d%d", &arg, &base);
    p = base;
    for (esp; p < arg; esp++)
    {
        p = base;
        for (int i = 1; i <= esp; i++)
        {

            p = p * base;
        }
    }
    if (p == arg)
        printf("Il logaritmo di %d in base %d    è %d\n", arg, base, esp);
    else
        printf("Il logaritmo di %d in base %d è compreso fra %d e %d.\n", arg, base, esp, esp--);
}