//Questa librerie include le funzioni relative alle liste collegate
#include <stdio.h>
#include <stdlib.h>

//la definizione di dato può essere ampliata come struct, ma alcune
//delle procedure/funzioni dovranno essere leggermente modificate
typedef struct
{
    double value;
    char check;
} Dato;

typedef struct nodo
{
    Dato dato;
    struct nodo *next;
} Nodo;

typedef Nodo *Lista;

//inizializza una lista collegata
void nuovaLista(Lista *l);

//Elimina una lista
void eliminaLista(Lista *l);

//controlla se la lista è vuota (utile per eseguire un check di "nuovaLista")
int vuota(Lista l);

//restituisce 0, inserita per motivi di simmetria
int piena(Lista l);

//inserisce il dato "n" in testa alla lista "l"
void insTesta(Lista *l, Dato n);

//inserisce "n" dati appartenenti al vettore "d" in testa alla lista "l"
void nInsTesta(Lista *l, Dato *d, int n);

//inserisce il dato "n" in modo ordinato nella lista "l"
void insOrdinato(Lista *l, Dato d);

//inserisce "n" dati appartenenti al vettore "d" in modo ordinato nella lista "l"
void nInsOrdinato(Lista *l, Dato *d, int n);

//inserisce il dato "n" in coda alla lista "l"
void insCoda(Lista *l, Dato d);

//inserisce "n" dati appartenenti al vettore "d" in coda alla lista "l"
void nInsCoda(Lista *l, Dato *d, int n);

//ordina la lista "l"
void ordina(Lista *l);

//elimina la testa alla lista "l"
void eliminTesta(Lista *l);

//elimina il primo elemento della lista "l" contenente il dato "d"
int elimina(Lista *l, Dato d);

//elimina tutti gli elementi della lista "l" contenenti il dato "d"
int eliminaTutti(Lista *l, Dato d);

//stampa una lista "l"
void stampa(Lista l);

//inverte l'ordine degli elementi di una lista "l"
// es. se la lista è [1, 2, 3] diventa [3, 2, 1]
Lista reverse(Lista l);

//restituisce la lunghezza della lista "l"
int lunghezza(Lista l);

//cerca il dato "d" nella lista "l", restituendo l'indirizzo del primo nodo
//che soddisfa la ricerca (es. se l = [1, 2, 3] e d = 2, return = [2, 3])
Lista *cerca(Lista *l, Dato d);

//modifica della funzione "cerca", per operare nella funzione "insOrdinato"
Lista *cercaOrdinato(Lista *l, Dato d);

//modifica della funzione "cerca", per operare nella funzione "insCoda"
Lista *cercaCoda(Lista *l);