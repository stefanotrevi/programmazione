#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define dim 50

int main(void)
{
    FILE *f;
    char s[dim];
    f = fopen("test.txt", "wt");

    if (f == NULL)
    {
        printf("Impossibile aprire il file\n");
        exit(-1);
    }
    do
    {
        scanf("%s", s);
        fprintf(f, "%s\n", s);
    } while (strcmp(s, "FINE") != 0);

    if (fclose(f) != 0)
    {
        printf("Impossibile chiudereil file!\n");
        exit(-2);
    }
    return 0;
}