#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define RED "\x1b[91m"
#define GREEN "\x1b[92m"
#define YELLOW "\x1b[93m"
#define BLUE "\x1b[94m"
#define MAGENTA "\x1b[95m"
#define CYAN "\x1b[96m"
#define RESET "\x1b[0m"

void printMatrix(int **M, int righe, int colonne, int el_of_in);
void intmMalloc(int ***n, unsigned int righe, unsigned int colonne);
void floatCsort(float **v, int dim);
void stampaVettore(void *v, int dim, int el_of_in, char type);
void copiaVettore(int *dest, int *src, int dim);
void ordinaMatrice(int **M, int righe, int colonne);
void ordinaMParz(int **M, int righe, int c);
void mFree(void **ptr, int righe);
void floatToMatrix(int **M, float *v, int dim);
void matrixToFloat(int **M, float **v, int dim);
void fRNG(float *out, float min, float max, int n, int seed);
int verificaOrdinato(float *v, int dim);
int compare(const void *a, const void *b);

int main(void)
{
    float *v;
    int base = 100000, i, dim, step = 10000, n = 100;
    char c;
    clock_t start, end;
    FILE *fT = fopen("test.txt", "wt");

    if (fT == NULL)
        exit(1);
    dim = base;
    for (i = 0; i < n; i++)
    {
        v = malloc(dim * sizeof(*v));
        fRNG(v, 1, dim, dim, 1);
        start = clock();
        floatCsort(&v, dim);
        //qsort(v, dim, sizeof(float), compare);
        end = clock();
        printf("%d\t%f\t%d\n", dim, (float)(end - start) / CLOCKS_PER_SEC, verificaOrdinato(v, dim));
        fprintf(fT, "%d\t%f\n", dim, (float)(end - start) / CLOCKS_PER_SEC);
        //stampaVettore(v, dim, -1, 'f');
        dim += step;
        free(v);
    }
    fclose(fT);
    printf("Premere un tasto per uscire:\n");
    scanf("%c", &c);
    return 0;
}

void floatCsort(float **v, int dim)
{
    int **M, i, j, k, l, len = 8 * sizeof(float), *temp, *sv, flag, z;

    //Creo la matrice dei bit e la matrice di supporto
    intmMalloc(&M, dim, len);

    //Copio i bit nelle celle della matrice
    floatToMatrix(M, *v, dim);

    //Ordino
    ordinaMatrice(M, dim, len);

    //Rimetto i numeri nell'array
    matrixToFloat(M, v, dim);
    mFree((void **)M, dim);
}

void intmMalloc(int ***n, unsigned int righe, unsigned int colonne)
{
    int i;

    *n = (int **)malloc(righe * sizeof(int *));
    for (i = 0; i < righe; i++)
        (*n)[i] = (int *)malloc(colonne * sizeof(int));
}

void printMatrix(int **M, int righe, int colonne, int el_of_in)
{
    int i, j;
    for (i = 0; i < righe; i++)
        stampaVettore(M[i], colonne, el_of_in, 0);
    printf("\n");
}

void stampaVettore(void *v, int dim, int el_of_in, char type)
{
    int i;

    switch (type)
    {
    case 'i':
        for (i = 0; i < dim; i++)
            if (i != el_of_in)
                printf("%d ", ((int *)v)[i]);
            else
                printf(RED "%d " RESET, ((int *)v)[i]);
        printf("\n");
        break;
    case 'f':
        for (i = 0; i < dim; i++)
            if (i != el_of_in)
                printf("%f ", ((float *)v)[i]);
            else
                printf(RED "%f " RESET, ((float *)v)[i]);
        printf("\n");
        break;
    default:
        v = (int *)v;
        for (i = 0; i < dim; i++)
            if (i != el_of_in)
                printf("%d", ((int *)v)[i]);
            else
                printf(RED "%d" RESET, ((int *)v)[i]);
        printf("\n");
        break;
    }
}

void ordinaMatrice(int **M, int righe, int colonne)
{
    int **Ms, i, j, k, flag = 1;

    ordinaMParz(M, righe, 0);
    /*
    intmMalloc(&Ms, righe, colonne);
    for (i = colonne - 1; i >= 0; i--)
    {

        if (flag)
        {
            ordinaMParz(Ms, M, righe, colonne, i);
            flag = 0;
        }
        else
        {
            ordinaMParz(M, Ms, righe, colonne, i);
            flag = 1;
        }
    }
    mFree((void **)Ms, righe);
    */
}
/*
void ordinaMParz(int **dest, int **src, int righe, int colonne, int i)
{
    int j, k = 0;
    for (j = 0; j < righe; j++)
        if (!src[j][i])
            copiaVettore(dest[j - k], src[j], colonne);
        else
            k++;
    k = j - k;
    for (j = 0; j < righe; j++)
        if (src[j][i])
        {
            copiaVettore(dest[k], src[j], colonne);
            k++;
        }
}
*/

void ordinaMParz(int **M, int righe, int c)
{
    int i, j, k = 1, *temp, z = 0;

    if (c < 32 && righe > 1)
    {
        for (i = 0; i < righe; i++)
            if (M[i][c])
            {
                for (j = i + k; j < righe; j++)
                    if (!M[j][c])
                    {
                        z++;
                        temp = M[i];
                        M[i] = M[j];
                        M[j] = temp;
                        break;
                    }
                    else
                        k++;
                if (j >= righe)
                    break;
            }
            else
                z++;
        ordinaMParz(M, z, c + 1);
        ordinaMParz(&M[z], righe - z, c + 1);
    }
}

void copiaVettore(int *dest, int *src, int dim)
{
    int i;
    for (i = 0; i < dim; i++)
        dest[i] = src[i];
}

void mFree(void **ptr, int righe)
{
    int i;

    for (i = 0; i < righe; i++)
        free(ptr[i]);
    free(ptr);
}

void floatToMatrix(int **M, float *v, int dim)
{
    int i, j, *sv, len = 8 * sizeof(*v);

    //Cast del puntatore
    sv = (int *)v;

    for (i = 0; i < dim; i++)
        for (j = len - 1; j >= 0; j--)
            M[i][j] = (sv[i] & (1 << (len - 1 - j))) >> (len - 1 - j);
}

void matrixToFloat(int **M, float **v, int dim)
{
    int i, j, *sv, len = 8 * sizeof(**v);

    sv = calloc(dim, sizeof(*sv));

    for (i = 0; i < dim; i++)
        for (j = len - 1; j >= 0; j--)
            sv[i] += M[i][j] << (len - 1 - j);
    free(*v);
    *v = (float *)sv;
}

void fRNG(float *out, float min, float max, int n, int seed)
{
    int i;

    if (seed)
        srand(seed);
    else
        srand(time(NULL));
    for (i = 0; i < n; i++)
    {
        out[i] = (float)(rand() % 100) / ((100 / max) + 0.1);
        if (out[i] < min)
            i--;
    }
}

int verificaOrdinato(float *v, int dim)
{
    int i;

    for (i = 1; i < dim; i++)
        if (v[i] < v[i - 1])
            return 0;
    return 1;
}

int compare(const void *a, const void *b)
{
    float fa = *(const float *)a, fb = *(const float *)b;

    return (fa > fb) - (fa < fb);
}