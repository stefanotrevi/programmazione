#include <stdio.h>
#include <time.h>
#include <Windows.h>
#include "../librerie/commonlib.h"

#define pi 3.1415926535897932384626433832795

int main(void)
{

    long long int start, end;

    start = WinCrono();
    wait(2000, 0);
    end = WinCrono();
    printf("%lf", (double)(end - start) / 10000);
    
    return 0;
}