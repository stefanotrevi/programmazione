#include <stdio.h>
#include <time.h>
#include <math.h>
#include "../librerie/commonlib.h"
#define NCICLI 10

int compare(const void *a, const void *b);

int main(void)
{
    int *v, vDim2;
    long long int start, end, elapsed, elapT[NCICLI];
    unsigned int i, j, k, vDim, nCicli, elapTime, spV[NCICLI], speed, offset = 15, tics = 10000000, base = 2;
    float theoT;
    float *fv;
    char fName[] = "risultati.txt";
    FILE *fT;

    fT = openFile(fName, "wt");
    printf("Si prega di attendere e non chiudere il programma, l'esecuzione potrebbe richiedere un paio di minuti.\nProgresso:\n");
    for (i = 0; i < NCICLI; i++)
    {
        for (j = 0; j < NCICLI; j++)
        {
            vDim = pow(base, offset + j);
            v = malloc(vDim * sizeof(fv));
            RNG(v, 0, vDim, vDim, 1);
            start = WinCrono();
            //countSort(v, vDim, 0, vDim);
            ordinaRicorsivo(v, vDim);
            //qsort(v, vDim, sizeof(int), compare);
            end = WinCrono();
            elapsed = end - start;
            elapT[j] = (elapT[j] * i + elapsed) / (i + 1);
            free(v);
            printf(".");
        }
        if (i)
            speed = (speed * i + vDim * ((double)tics / (double)elapsed)) / (i + 1);
        else
            speed = vDim * ((double)tics / (double)elapsed);
        printf(" %d%%\n", (i + 1) * 100 / NCICLI);
    }

    fprintf(fT, "--------RISULTATI ALGORITMO DI ORDINAMENTO--------\n");
    fprintf(fT, "Iterazioni eseguite: %d\nVelocità ordinamento: %u item/s\nTipo di aumento dimensione vettore: %d^x\n\n", NCICLI, speed, base);
    for (i = 0; i < NCICLI - 1; i++)
    {
        if (!elapT[i])
            elapT[i]++;
        vDim = pow(base, i + offset);
        vDim2 = base * vDim;
        theoT = (vDim2 * log(vDim2) / log(base)) / (vDim * log(vDim) / log(base));
        fprintf(fT, "Elementi: 2^%d\nTempo medio: %lf ms\n", i + offset, (double)elapT[i] / 10000);
        fprintf(fT, "Rapporto tempi: %f\nRapporto tempi teorico:%f\n\n", (double)elapT[i + 1] / elapT[i], theoT);
    }
    vDim = pow(base, i + offset);
    vDim2 = base * vDim;
    fprintf(fT, "Elementi: 2^%d\nTempo medio: %lf ms\n", i + offset, (double)elapT[i] / 10000);
    printf("File %s generato.\n", fName);
    quit();
    return 0;
}

int compare(const void *a, const void *b)
{
    int fa = *(const int *)a, fb = *(const int *)b;

    return (fa > fb) - (fa < fb);
}