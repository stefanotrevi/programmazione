//scansiona una data, ne calcola il giorno giuliano, e stampa il giorno della settimana corrispondente
#include <stdio.h>

int main(void)
{
    //initialize main variables
    int JD, N0, N1, N2, N3, G, M, A;
    //initialize and assign constants
    int C0 = 32075, C1 = 14, C2 = 1461, C3 = 4800, C4 = 367;
    int C5 = 2, C6 = 12, C7 = 3, C8 = 4900, C9 = 400, C10 = 4;

    printf("Inserire giorno, mese ed anno attuali: ");
    scanf("%d%d%d", &G, &M, &A);

    N0 = (M - C1) / C6;
    N1 = C2 * (A + C3 + N0) / C10;
    N2 = C4 * (M - C5 - C6 * N0) / C6;
    N3 = C7 * (A + C8 + N0) / C9;
    JD = N1 + N2 - N3 + G - C0;

    printf("Il giorno giuliano attuale è: %d, ed è un ", JD);
    switch (JD % 7)
    {
    case 0:
        printf("Lunedì");
        break;
    case 1:
        printf("Martedì");
        break;
    case 2:
        printf("Mercoledì");
        break;
    case 3:
        printf("Giovedì");
        break;
    case 4:
        printf("Venerdì");
        break;
    case 5:
        printf("Sabato");
        break;
    default:
        printf("Domenica");
        break;
    }
    printf("\n");
}