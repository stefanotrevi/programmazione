//scansiona un numero e ne satmpa la somma con tutti i suoi precedenti
#include <stdio.h>

int main(void)
{

    int n, m;

    printf("Inserire numero a cui sommare tutti i sui precedenti: ");
    scanf("%d", &n);
    /* power of math...
    m = n * (n + 1) / 2;
    printf("%d\n", m); */

    m = n;
    for (int i = 0; i < n; i++)
    {
        m = m + i;
    }
    printf("%d\n", m);
}