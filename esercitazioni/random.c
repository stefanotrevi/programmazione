//esercizio sui numeri casuali
#include <stdio.h>
#define DIM 100
#define vDIM 31

int main(void)
{
    int i, j = 0, in, max, nMax, v[DIM], temp, flag = 0, avg = 0, freq[vDIM], lDIM = 0;
    float media;

    for (i = 0; i < DIM; i++)
    {
        scanf("%d", &in);
        if (in != -1)
        {
            v[i] = in;
            lDIM++;
        }
        else
            break;
    }

    for (i = 0; i < lDIM - 1; i++)
    {
        if (flag)
        {
            i = 0;
            flag = 0;
        }
        if (v[i + 1] < v[i])
        {
            temp = v[i];
            v[i] = v[i + 1];
            v[i + 1] = temp;
            flag = 1;
            i = 0;
        }
    }

    for (i = 0; i < lDIM; i++)
        avg = avg + v[i];

    media = avg / lDIM;

    for (i = 0; i < vDIM; i++)
        freq[i] = 0;

    for (i = 0; i < vDIM; i++)
    {
        for (j; j < lDIM; j++)
        {
            if (v[j] == i)
                freq[i]++;
            else
                break;
        }
    }

    for (i = 1, j = 0; i < vDIM - 1; i++)
        if (freq[j] > freq[i])
        {
            max = j;
            nMax = freq[j];
        }
        else
        {
            max = i;
            nMax = freq[i];
            j = i;
        }

    printf("Voto minimo: %d, voto massimo: %d, voto medio: %f, voto più frequente: %d, che hanno preso %d studenti", v[0], v[lDIM - 1], media, max, nMax);
    printf("\n");
    return 0;
}