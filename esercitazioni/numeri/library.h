typedef struct
{
    float r;
    float im;
} cplx;

typedef struct
{
    int num;
    int den;
} raz;

typedef struct
{
    enum
    {
        intero,
        razionale,
        reale,
        complesso
    } tipo_numero;

    union {
        int numero_intero;
        raz numero_razionale;
        float numero_reale;
        cplx numero_complesso;
    } valore;

} numero;

void getTipoNumero(numero *n);
void assegnaNumero(numero *n);
void stampaNumero(numero n);
