#include <stdio.h>
#include "library.h"

void getTipoNumero(numero *n)
{
    char tN;
    printf("Che tipo di numero si vuole immettere? (intero (i), razionale (q), reale (r), complesso (c))\n");
    do
    {
        scanf(" %c", &tN);
        if (tN != 'i' && tN != 'q' && tN != 'r' && tN != 'c')
            printf("Tipo di numero non valido, riprova. (tipi accettati: intero (i), razionale (q), reale (r), complesso (c))\n");
    } while (tN != 'i' && tN != 'q' && tN != 'r' && tN != 'c');
    if (tN == 'i')
        n->tipo_numero = intero;
    else if (tN == 'q')
        n->tipo_numero = razionale;
    else if (tN == 'r')
        n->tipo_numero = reale;
    else if (tN == 'c')
        n->tipo_numero = complesso;
}
void assegnaNumero(numero *n)
{
    int v;
    raz r;
    float f;
    cplx c;
    printf("Inserire numero:\n");
    switch (n->tipo_numero)
    {
    case (intero):
        scanf("%d", &v);
        n->valore.numero_intero = v;
        break;
    case (razionale):
        scanf("%d%d", &r.num, &r.den);
        n->valore.numero_razionale = r;
        break;
    case (reale):
        scanf("%f", &f);
        n->valore.numero_reale = f;
        break;
    case (complesso):
        scanf("%f%f", &c.r, &c.im);
        n->valore.numero_complesso = c;
        break;
    }
}
void stampaNumero(numero n)
{
    switch (n.tipo_numero)
    {
    case (intero):
        printf("%d\n", n.valore.numero_intero);
        break;
    case (razionale):
        printf("%d/%d\n", n.valore.numero_razionale.num, n.valore.numero_razionale.den);
        break;
    case (reale):
        printf("%f\n", n.valore.numero_reale);
        break;
    case (complesso):
        printf("%f + %fi\n", n.valore.numero_complesso.r, n.valore.numero_complesso.im);
        break;
    }
}