//prende i lati di un triangolo (senza verifica...) e calcola perimetro ed area
#include <stdio.h>

double media(double a, double b);
double radq(double a);
double perimetro(double a, double b, double c);
double area(double a, double b, double c);

double main(void)
{
    double a, b, c;

    printf("Inserire i tre lati del triangolo:\n");
    scanf("%lf%lf%lf", &a, &b, &c);
    printf("Perimetro = %lf\nArea = %lf\n", perimetro(a, b, c), area(a, b, c));
    return 0;
}

double media(double a, double b)
{
    double c;
    c = (a + b) / 2;
    return c;
}

double radq(double a)
{
    double i, b;
    for (i = 1; i <= 100; i++)
        b = media(b, a / b);
    return b;
}

double perimetro(double a, double b, double c)
{
    double P;
    P = a + b + c;
    return P;
}

double area(double a, double b, double c)
{
    float A, p;
    p = perimetro(a, b, c) / 2;
    A = radq(p * (p - a) * (p - b) * (p - c));
    return A;
}
