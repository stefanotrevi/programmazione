//scansiona una matrice, la stampa, e verifica se è simmetrica o no
#include <stdio.h>
#define DIM 3

void lettura(int M[][DIM]);
void stampa(int M[][DIM]);
int simm(int M[][DIM]);

int main(void)
{
    int M[DIM][DIM], sim;

    lettura(M);
    stampa(M);
    sim = simm(M);
    if (sim == 1)
        printf("La matrice è simmetrica\n");
    else
        printf("La matrice non è simmetrica\n");

    return 0;
}

void lettura(int M[][DIM])
{
    int i, j;
    for (i = 0; i < DIM; i++)
        for (j = 0; j < DIM; j++)
            scanf("%d", &M[i][j]);
}

void stampa(int M[][DIM])
{
    int i, j;

    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
            printf("%d ", M[i][j]);
        printf("\n");
    }
}

int simm(int M[][DIM])
{
    int i, j, simm = 1;
    for (i = 0; i < DIM && simm; i++)
        for (j = i + 1; j < DIM && simm; j++)
            if (M[i][j] != M[j][i])
                simm = 0;

    return simm;
}