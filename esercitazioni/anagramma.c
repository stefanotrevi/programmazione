#include <stdio.h>
#define DIM 30

int main(void)
{
    char v[DIM], w[DIM];
    int i = 0, j = 0, flag = 0, dlv = 0, dlw = 0;

    printf("Inserire una parola:\n");
    scanf("%s", v);
    printf("Inserirne un'altra:\n");
    scanf("%s", w);

    for (i = 0; i < DIM && v[i] != '\0'; i++)
        dlv = i + 1;
    for (i = 0; i < DIM && w[i] != '\0'; i++)
        dlw = i + 1;

    if (dlw != dlv)
        printf("Le due parole non sono anagrammi\n");
    else
    {
        for (i = 0; i < dlv; i++)
        {
            for (j = 0; j < dlv; j++)
            {
                if (v[j] == w[i] && v[j] != '\0' && w[i] != '\0')
                {
                    flag++;
                    w[i] = '\0';
                    v[j] = '\0';
                    break;
                }
            }
        }
        if (dlw == flag)
            printf("Le due parole sono anagrammi!\n");
        else
            printf("Le due parole non sono anagrammi\n");
    }
}