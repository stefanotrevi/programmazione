//trova e stampa tutti i numeri perfetti minori di n
#include <stdio.h>

int sommaDivisori(int n);

int main(void)
{
    int n;
    for (n = 1; n <= 10000; n++)
    {
        if (n == sommaDivisori(n))
            printf("%d\n", n);
    }
    return 0;
}

int sommaDivisori(int n)
{
    int s = 0, i;
    for (i = 2; i <= n; i++)
    {
        if (!(n % i))
            s = s + n / i;
    }
    return s;
}