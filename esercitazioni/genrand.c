//generatore di numeri "casuali"
#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    int i;
    for (i = 0; i < 300; i++)
        printf("%d\n", 0 + rand() % (100 + 1));

    printf("-1");
    return 0;
}