//confronta due numeri attraverso la funzione max(x, y)
#include <stdio.h>

int max(int a, int b)
{
    if (a > b)
        return a;
    else
        return b;
}

int main(void)
{
    int a, b;
    printf("Inserire due numeri:\n");
    scanf("%d%d", &a, &b);
    if (a == b)
        printf("I due numeri sono uguali\n");
    else
        printf("%d\n", max(a, b));
}