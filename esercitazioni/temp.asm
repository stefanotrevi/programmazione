; mark_description "Intel(R) C++ Intel(R) 64 Compiler for applications running on Intel(R) 64, Version 18.0.3.210 Build 20180410";
; mark_description "";
; mark_description "/FA";
	OPTION DOTNAME
_TEXT	SEGMENT      'CODE'
;	COMDAT main
TXTST0:
; -- Begin  main
;main	ENDS
_TEXT	ENDS
_TEXT	SEGMENT      'CODE'
;	COMDAT main
; mark_begin;
       ALIGN     16
	PUBLIC main
; --- main(void)
main	PROC 
.B1.1::                         ; Preds .B1.0
                                ; Execution count [1.00e+000]
L1::
                                                           ;3.1
        sub       rsp, 40                                       ;3.1
        xor       edx, edx                                      ;3.1
        mov       ecx, 3                                        ;3.1
        call      __intel_new_feature_proc_init                 ;3.1
                                ; LOE rbx rbp rsi rdi r12 r13 r14 r15 xmm6 xmm7 xmm8 xmm9 xmm10 xmm11 xmm12 xmm13 xmm14 xmm15
.B1.4::                         ; Preds .B1.1
                                ; Execution count [1.00e+000]
        stmxcsr   DWORD PTR [32+rsp]                            ;3.1
        xor       eax, eax                                      ;5.9
        or        DWORD PTR [32+rsp], 32832                     ;3.1
        ldmxcsr   DWORD PTR [32+rsp]                            ;3.1
        add       rsp, 40                                       ;5.9
        ret                                                     ;5.9
        ALIGN     16
                                ; LOE
.B1.2::
; mark_end;
main ENDP
;main	ENDS
_TEXT	ENDS
.xdata	SEGMENT  DWORD   READ  ''
;	COMDAT .xdata$main
	ALIGN 004H
.unwind.main.B1_B4	DD	66561
	DD	16900
;.xdata$main	ENDS
.xdata	ENDS
.pdata	SEGMENT  DWORD   READ  ''
;	COMDAT .pdata$main
	ALIGN 004H
.pdata.main.B1_B4	DD	imagerel .B1.1
	DD	imagerel .B1.2
	DD	imagerel .unwind.main.B1_B4
;.pdata$main	ENDS
.pdata	ENDS
_DATA	SEGMENT      'DATA'
_DATA	ENDS
; -- End  main
_DATA	SEGMENT      'DATA'
_DATA	ENDS
EXTRN	__intel_new_feature_proc_init:PROC
EXTRN	__ImageBase:PROC
	INCLUDELIB <libmmt>
	INCLUDELIB <LIBCMT>
	INCLUDELIB <libirc>
	INCLUDELIB <svml_dispmt>
	INCLUDELIB <OLDNAMES>
	INCLUDELIB <libdecimal>
	END
