//calcola la radice quadrata di un numero usando il "metodo babilonese"
#include <stdio.h>
#include <math.h>

double media(double a, double b);
double radq(double a, double b, int esp);

int main(void)
{
    double a, b = 1;
    int i, esp;

    printf("inserire numero da valutare:\n");
    scanf("%lf", &a);
    printf("Inserire le cifre significative che si vogliono ottenere (1-6) (NOTA! Una precisione maggiore richiederà un tempo di calcolo maggiore!):\n");
    scanf("%d", &esp);

    if (a > 0)
        printf("%lf\n", radq(a, b, esp));
    else
        printf("%lfi\n", radq(-a, b, esp));

    return 0;
}
double media(double a, double b)
{
    double c;
    c = (a + b) / 2;
    return c;
}

double radq(double a, double b, int esp)
{
    int i;
    for (i = 1; fabs(b * b - a) >= pow(10, -esp); i++)
        b = media(b, a / b);
    return b;
}