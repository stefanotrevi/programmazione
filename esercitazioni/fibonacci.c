#include <stdio.h>
#define DIM 10000

void fibonacci(int n)
{
    long long int a[DIM], i;
    a[0] = 0;
    a[1] = 1;
    for (i = 2; i <= n; i++)
    {
        a[i] = a[i - 2] + a[i - 1];
    }
    printf("%d\n", a[i-1]);
}

int main(void)
{
    int n;
    printf("Inserire numero:\n");
    scanf("%d", &n);
    fibonacci(n);
}