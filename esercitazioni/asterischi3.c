//simile ad asterischi2, ma stampa a tabella
#include <stdio.h>

int riga(int m)
{
    int i;
    for (i = 0; i < m; i++)
        printf("*");
}

int main(void)
{
    int m, n, i;

    printf("Inserire numero di righe e di colonne:\n");
    scanf("%d%d", &n, &m);

    for (i = 0; i < n; i++)
    {
        riga(m);
        printf("\n");
    }
}