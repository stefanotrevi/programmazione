//programma per sommare stringhe fra di loro
#include <stdio.h>
#define DIM 5

int main(void)
{
    int i;
    char s1[DIM] = "gian", s2[DIM] = "luca", s3[20];

    do
    {
        if (i <= DIM - 2)
            s3[i] = s1[i];
        else
            s3[i] = s2[i + 1 - DIM];
        i++;
    } while (s3[i - 1] != '\0' && i < 20);

    printf("%s\n", s3);
    return 0;
}