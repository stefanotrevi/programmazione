//scansiona una stringa ed un carattere, e controlla se il carattere compare nella stringa
//stampandone numero di volte ed eventuale posizione/i
#include <stdio.h>
#define DIM 20

int main(void)
{
    int i, j = 0, n[DIM];
    char c, s[DIM];

    printf("Inserire stringa, seguita dal carattere da cercare:\n");
    scanf("%s %c", s, &c);

    for (i = 0; s[i] != '\0' && i < DIM; i++)
        if (s[i] == c)
        {
            n[j] = i + 1;
            j++;
        }
    if (j)
    {
        printf("Il carattere %c compare %d volte in posizione: ", c, j);
        for (i = 0; i < j; i++)
            printf("%d ", n[i]);
        printf("\n");
    }
    else
        printf("Il carattere non compare\n");
    return 0;
}