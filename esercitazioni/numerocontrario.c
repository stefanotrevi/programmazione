//scansiona un numero e ne inverte le cifre
#include <stdio.h>

int main(void)
{

    int a, b;
    printf("Inserire numero: ");
    scanf("%d", &a);

    do
    {
        b = a % 10;
        a = a / 10;
        printf("%d", b);
    } while (a > 0);
    printf("\n");
}