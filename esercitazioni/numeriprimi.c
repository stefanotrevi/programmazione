//verifica se un numero è primo o no
#include <stdio.h>

int main(void)
{
    int n, i = 2;

    scanf("%d", &n);

    while (i < n)
    {
        if (n % i != 0)
            i++;
        else
            i = n + 2;
    }
    if (i == n + 2)
        printf("Il numero non è primo\n");
    else
        printf("Il numero è primo\n");
}