#include "listino.h"
#include <stdlib.h>
#include <stdio.h>

void azzeraListino(listino *l)
{
    l->nElementi = 0;
}

void aggiungiProdotto(listino *l, prodotto p)
{
    if (l->nElementi < MAX)
    {
        l->p[l->nElementi] = p;
        l->nElementi++;
    }
    else
        printf("Numero elementi massimo raggiunto, impossibile aggiungerne altri!\n");
}
