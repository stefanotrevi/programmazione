#include <stdio.h>
#include <stdlib.h>
#include "listino.h"

void intro(void);
void getAcquisti(int c[MAX], int *nPI, listino l);
void calcolaScontrino(int c[MAX], int qP[MAX], int nPI, listino l);
void stampaScontrino(FILE *ft, int qP[MAX], listino l);

int main(void)
{
    listino l;
    prodotto p;
    int c[MAX] = {0}, qP[MAX] = {0}, nPI = 0;
    FILE *fb, *ft;

    intro();
    fb = fopen("listino.dat", "rb");
    ft = fopen("scontrino.txt", "wt");
    if (fb == NULL)
    {
        printf("File non esistente!\n");
        exit(-2);
    }

    azzeraListino(&l);
    while (fread(&p, sizeof(prodotto), 1, fb))
        aggiungiProdotto(&l, p);

    getAcquisti(c, &nPI, l);
    calcolaScontrino(c, qP, nPI, l);
    stampaScontrino(ft, qP, l);
    return 0;
}

void intro(void)
{
    //solo per windows CMD
    system("chcp 65001");
    //solo per Windows Powershell
    system("[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8");
    system("cls");
}
void getAcquisti(int c[MAX], int *nPI, listino l)
{
    int temp;
    printf("Inserire codici prodotti acquistati (da 0 a %d), -1 per terminare.\n", l.nElementi - 1);
    do
    {
        scanf("%d", &temp);
        if (temp < -1 || temp >= l.nElementi)
            printf("L'elemento %d non è valido e non è stato considerato\n", temp);
        else if (temp != -1)
        {
            c[*nPI] = temp;
            *nPI = *nPI + 1;
        }
    } while (temp != -1);
}

void calcolaScontrino(int c[MAX], int qP[MAX], int nPI, listino l)
{
    int i, j;
    for (i = 0; i < l.nElementi; i++)
        for (j = 0; j < nPI; j++)
        {
            if (c[j] == l.p[i].codice)
                qP[i] = qP[i] + 1;
        }
}

void stampaScontrino(FILE *ft, int qP[MAX], listino l)
{
    int i;
    float totale = 0;
    for (i = 0; i < l.nElementi; i++)
    {
        fprintf(ft, "%d x %f (%s) = %f", qP[i], l.p[i].prezzo, l.p[i].descrizione, qP[i] * l.p[i].prezzo);
        totale += qP[i] * l.p[i].prezzo;
        fprintf(ft, "\n");
    }
    fprintf(ft, "Totale: %f", totale);
    fprintf(ft, "\n");
}
