#define DIM 30
#define MAX 100

typedef struct
{
    int codice;
    char descrizione[DIM];
    float prezzo;
} prodotto;

typedef struct
{
    int nElementi;
    prodotto p[MAX];
} listino;

typedef struct
{
    int qP;

} scontrino;

void azzeraListino(listino *l);
void aggiungiProdotto(listino *l, prodotto p);