//scansiona un lato e calcola l'area del quadrato corrispondente
#include <stdio.h>
#include <math.h>

int main(void)
{
    int a=1;
    int b;
    printf("Quanto misura il lato? ");
    scanf("%d", &a);
    b = (int) pow (a, 2);
    printf("L'area misura %d\n", b);
}