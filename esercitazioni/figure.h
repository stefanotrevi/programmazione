#include <math.h>

typedef enum {
    quadrato,
    cerchio,
    rettangolo,
    triangolo
} TipoFigura;

typedef struct
{
    TipoFigura tipo;
    union {
        struct
        {
            float lato;
        } dati_quadrato;
        struct
        {
            float raggio;
        } dati_cerchio;
        struct
        {
            float base;
            float altezza;
        } dati_rettangolo;
        struct
        {
            float lato1;
            float lato2;
            float lato3;
        } dati_triangolo;
    } dato;
} Figura;

Figura Quadrato(float lato)
{
    Figura f;
    f.tipo = quadrato;
    f.dato.dati_quadrato.lato = lato;
    return f;
}

Figura Cerchio(float raggio)
{
    Figura f;
    f.tipo = cerchio;
    f.dato.dati_cerchio.raggio = raggio;
    return f;
}

Figura Rettangolo(float base, float altezza)
{
    Figura f;
    f.tipo = rettangolo;
    f.dato.dati_rettangolo.base = base;
    f.dato.dati_rettangolo.altezza = altezza;
    return f;
}

Figura Triangolo(float lato1, float lato2, float lato3)
{
    Figura f;
    f.tipo = triangolo;
    f.dato.dati_triangolo.lato1 = lato1;
    f.dato.dati_triangolo.lato2 = lato2;
    f.dato.dati_triangolo.lato3 = lato3;
    return f;
}

float area(Figura f)
{
    switch (f.tipo)
    {
    case quadrato:
        return f.dato.dati_quadrato.lato * f.dato.dati_quadrato.lato;
    case cerchio:
        return 3.14159265 * f.dato.dati_cerchio.raggio * f.dato.dati_cerchio.raggio;
    case rettangolo:
        return f.dato.dati_rettangolo.base * f.dato.dati_rettangolo.altezza;
    case triangolo:
    {
        float p = (f.dato.dati_triangolo.lato1 + f.dato.dati_triangolo.lato2 + f.dato.dati_triangolo.lato3) / 2;
        return sqrt(p * (p - f.dato.dati_triangolo.lato1) * (p - f.dato.dati_triangolo.lato2) * (p - f.dato.dati_triangolo.lato3));
    }
    }
}