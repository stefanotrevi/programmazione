//controlla se due frazioni sono identiche
#include <stdio.h>

int main(void)
{

    //dobbiamo dimostrare l'equivalenza di due frazioni, inizializziamo dunque due "verificatori"
    int a, b, c, d, ver1, ver2;

inizio:
    printf("Inserire le frazioni da confrontare, senza il simbolo di divisione: ");
    scanf("%d%d%d%d", &a, &b, &c, &d);

    ver1 = a * d;
    ver2 = b * c;

    if (b == 0 || d == 0)
    {
        printf("Una delle due frazioni non ha senso, riprova.\n");
        goto inizio;
    }

    if (ver1 == ver2)
        printf("Le due frazioni sono equivalenti.\n");
    else
        printf("Le due frazioni non sono equivalenti.\n");
}