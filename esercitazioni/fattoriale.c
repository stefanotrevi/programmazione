//attraverso la funzione fattoriale(x), scansiona un numero e ne stampa il fattoriale
#include <stdio.h>

int fattoriale(int a)
{
    int b = a, i;
    for (i = 1; i < b; i++)
        a = a * i;
    return a;
}

int main(void)
{
    int a;

    printf("Inserire numero:\n");
    scanf("%d", &a);
    printf("%d\n", fattoriale(a));
}