#include <stdio.h>
#include "figure.h"
#include <math.h>

int main(void)
{
    Figura q, c, r, t;

    q = Quadrato(5.0);
    c = Cerchio(5.0);
    r = Rettangolo(5.0, 2.0);
    t = Triangolo(3.0, 4.0, 5.0);

    printf("%f\n%f\n%f\n%f\n", area(q), area(c), area(r), area(t));

    return 0;
}