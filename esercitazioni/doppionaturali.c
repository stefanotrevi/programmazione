//scansione un numero e lo raddoppia
#include <stdio.h>

int main(void)
{

    int a;

start:
    printf("inserire numero naturale da raddoppiare: ");
    scanf("%d", &a);

    if (a >= 0)
    {
        printf("%d\n", 2 * a);
        goto start;
    }
    else
        printf("Programma terminato\n");
}