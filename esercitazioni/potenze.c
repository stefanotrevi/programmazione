//scansiona base ed esponente e ne calcola la potenza
#include <stdio.h>

int main(void)
{

    int base, arg, esp, err = 0;

    printf("Inserire base ed esponente: ");
    scanf("%d%d", &base, &esp);
    arg = base;
    if (esp == 0 && base == 0)
        err = 1;
    else if (esp == 0)
        arg = 1;
    for (int i = 1; i < esp; i++)
    {

        arg = arg * base;
    }
    if (err)
        printf("NaN\n");
    else
        printf("%d\n", arg);
}