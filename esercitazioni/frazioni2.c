#include <stdio.h>

typedef struct
{
    int num;
    int den;
} Frazione;

void leggiFrazione(Frazione *f);
void stampaFrazione(Frazione f);
Frazione dividiFrazioni(Frazione a, Frazione b);

int main(void)
{
    Frazione a, b, q;
    leggiFrazione(&a);
    leggiFrazione(&b);
    q = dividiFrazioni(a, b);
    if (q.den)
        stampaFrazione(q);
    else
        printf("DivideByZero\n");
    return 0;
}

void leggiFrazione(Frazione *f)
{
    scanf("%d%d", &(*f).num, &(*f).den);
}

void stampaFrazione(Frazione f)
{
    printf("%d/%d\n", f.num, f.den);
}

Frazione dividiFrazioni(Frazione a, Frazione b)
{
    Frazione q;
    if (b.num != 0 && a.den != 0 && b.den != 0)
    {
        q.num = a.num * b.den;
        q.den = a.den * b.num;
    }
    else
    {
        q.num = 1;
        q.den = 0;
    }
    return q;
}