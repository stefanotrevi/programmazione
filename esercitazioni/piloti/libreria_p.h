#include <stdio.h>
#include <stdlib.h>
#define PMAX 30
#define NMAX 51
#define GMAX 20
#define NF 10

typedef struct
{
    char nome[PMAX];
    int stats[GMAX];
    int punteggio;
} pilota;

void ordinaPiloti(pilota v[PMAX], int np)
{
    int i, flag = 1;
    pilota temp;

    for (i = 0; i < np - 1; i++)
    {
        if (flag)
        {
            i = 0;
            flag = 0;
        }
        if (v[i + 1].punteggio < v[i].punteggio)
        {
            temp = v[i];
            v[i] = v[i + 1];
            v[i + 1] = temp;
            flag = 1;
            i = 0;
        }
    }
}
void intro(void)
{
    char start;
    //solo per windows CMD
    system("chcp 65001");
    //solo per Windows Powershell
    system("[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8");
    system("cls");
}
void openBinary(FILE *fb, int posizioni[][PMAX], pilota listaP[PMAX], int *np, int *ng)
{
    int i, j;
    fb = fopen("PILOTI.dat", "rb");

    if (fb == NULL)
    {
        printf("File .dat non trovato\n");
        exit(1);
    }

    fread(np, sizeof(int), 1, fb);
    for (i = 0; i < *np; i++)
    {
        pilota temp;
        fread(temp.nome, sizeof(char), NMAX, fb);
        for (j = 0; j < NMAX; j++)
            listaP[i].nome[j] = temp.nome[j];
    }
    fread(ng, sizeof(int), 1, fb);
    for (i = 0; i < *ng; i++)
        for (j = 0; j < *np; j++)
        {
            int temp;
            fread(&temp, sizeof(int), 1, fb);
            if (temp)
                posizioni[i][j] = temp;
            else
                j--;
        }
    fclose(fb);
}

void openText(FILE *fp, int puntiP[NF], int *nf)
{
    int i, temp;
    if (fp == NULL)
    {
        printf("File .txt non trovato\n");
        exit(1);
    }
    for (i = 0; i < NF; i++)
        puntiP[i] = 0;
    fp = fopen("punteggi.txt", "rt");

    for (i = 0; i < NF && temp != 1; i++)
    {
        fscanf(fp, "%d", &temp);
        puntiP[i] = temp;
    }
    *nf = i;
    fclose(fp);
}
void stampaClassifica(int np, pilota listaP[PMAX])
{
    int i;
    for (i = np - 1; i >= 0; i--)
        printf("%s: %d\n", listaP[i].nome, listaP[i].punteggio);
}

void creaStats(int np, int nf, int ng, int posizioni[][PMAX], pilota listaP[PMAX], int puntiP[NF])
{
    int i, j, k;
    for (i = 0; i < np; i++)
    {
        listaP[i].punteggio = 0;
        for (j = 0; j < nf; j++)
            listaP[i].stats[j] = 0;
    }

    for (i = 0; i < np; i++)         //pilota
        for (j = 0; j < ng; j++)     //giorno
            for (k = 0; k < nf; k++) //posizione
                if (posizioni[j][i] == k + 1)
                    listaP[i].stats[k]++;
    for (i = 0; i < np; i++)
        for (j = 0; j < nf; j++)
            listaP[i].punteggio += puntiP[j] * listaP[i].stats[j];
}
