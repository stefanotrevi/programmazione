#include <stdio.h>
#include <stdlib.h>
#include "libreria_p.h"

int main(void)
{
    int np, i, j, k, l, ng, nf, posizioni[GMAX][PMAX], puntiP[NF];
    pilota listaP[PMAX];
    FILE *fb, *fp;

    intro();
    openBinary(fb, posizioni, listaP, &np, &ng);
    openText(fp, puntiP, &nf);
    creaStats(np, nf, ng, posizioni, listaP, puntiP);
    ordinaPiloti(listaP, np);
    stampaClassifica(np, listaP);

    return 0;
}