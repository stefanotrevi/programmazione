#include <stdio.h>
#define DIM 100

void lettura(int a[], int dl);
void ordinamento(int v[], int dl);
int repeated(int v[], int dl);
int deleteRepeat(int v[], int dl);

int main(void)
{
    int v[DIM], dl, rp, i;
    printf("Inserire numero di elementi:\n");
    scanf("%d", &dl);
    rp = repeated(v, dl);
    if (rp)
    {
        printf("Ci sono elementi ripetuti. Inizio procedura eliminazione delle ripetizioni.\n");
        dl = deleteRepeat(v, dl);
        printf("Il nuovo array è:\n");
        for (i = 0; i < dl; i++)
            printf("%d ", v[i]);
        printf("\n");
    }
    else
        printf("Non ci sono elementi ripetuti\n");
    return 0;
}

void lettura(int a[], int dl)
{
    int i;
    printf("Inserire i valori:\n");
    for (i = 0; i < dl; i++)
        scanf("%d", &a[i]);
}

void ordinamento(int v[], int dl)
{
    int i, flag = 1, temp;

    lettura(v, dl);

    for (i = 0; i < dl - 1; i++)
    {
        if (flag)
        {
            i = 0;
            flag = 0;
        }
        if (v[i + 1] < v[i])
        {
            temp = v[i];
            v[i] = v[i + 1];
            v[i + 1] = temp;
            flag = 1;
            i = 0;
        }
    }
}

int repeated(int v[], int dl)
{
    int i, rp = 0;
    ordinamento(v, dl);
    for (i = 1; i < dl; i++)
        if (v[i] == v[i - 1])
        {
            rp = 1;
            break;
        }
    return rp;
}

int deleteRepeat(int v[], int dl)
{
    int i, j, ndl;
    for (i = 1; i < dl; i++)
        if (v[i] == v[i - 1])
        {
            dl--;
            for (j = i; j < dl; j++)
                v[j - 1] = v[j];
        }
    ndl = dl;
    return ndl;
}
