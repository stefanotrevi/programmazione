//scansiona un numero e lo stampa dopo aver applicato la funzione PI
#include <stdio.h>
int pi(int n);

int main(void)
{
    int n;
    printf("Inserire valore per cui calcolare la funzione PI:\n");
    scanf("%d", &n);
    printf("%d\n", pi(n));

    return 0;
}

int pi(int n)
{

    int b, i, j, f = 0, c = 0;

    for (i = 2; i <= n; i += 2)
    {
        b = i;
        j = 2;
        while (j <= b)
        {
            if (j == b)
            {
                c++;
                b--;
            }
            if (b % j != 0)
                if (j == 2)
                    j++;
                else
                    j += 2;
            else
                break;
        }
        if (i == 2)
            i--;
    }
    return c;
}