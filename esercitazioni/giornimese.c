//scansiona una data (mese ed anno) e stampa il numero di giorni di quel mese, controllando se l'ano è bisestile
#include <stdio.h>

int main(void)
{

    int anno, mese, giorni, ds1 = 4, ds2 = 100, ds3 = 400, val1, val2, val3;

input:
    printf("Inserire mese ed anno: ");
    scanf("%d%d", &mese, &anno);

    val1 = anno % ds1;
    val2 = anno % ds2;
    val3 = anno % ds3;

    if (mese <= 0 || mese > 12)
    {
        printf("Il mese inserito non è valido, per favore scegli un mese fra 1 e 12\n");
        goto input;
    }
    else if (mese == 11 || mese == 4 || mese == 6 || mese == 9)
        printf("Nel mese %d dell'anno %d ci sono 30 giorni\n", mese, anno);
    else if (mese != 2)
        printf("Nel mese %d dell'anno %d ci sono 31 giorni\n", mese, anno);
    else
    {
        if (val1 == 0 && val2 != 0 || val3 == 0)
            printf("Nel mese %d dell'anno %d ci sono 29 giorni\n", mese, anno);
        else
            printf("Nel mese %d dell'anno %d ci sono 28 giorni\n", mese, anno);
    }
}