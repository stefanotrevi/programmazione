//scansiona i coefficienti di un polinomio di grado 2 e stampa le coordinate (X, Y), a passi, della corrispondente parabola
#include <stdio.h>
#define D 21

float parabola(float a, float b, float c, float x);

int main(void)
{
    float X[D], Y[D], a, b, c;
    float start = -1, step = 0, step0;
    int i;

    printf("Inserire i coefficienti a, b e c:\n");
    scanf("%f%f%f", &a, &b, &c);
    printf("Inserire lo step:\n");
    scanf("%f", &step0);

    for (i = 0; i < D; i++)
    {
        X[i] = start + step;
        Y[i] = parabola(a, b, c, X[i]);
        printf("p%d = (%f, %f)\n", i, X[i], Y[i]);
        step = step + step0;
    }
    return 0;
}

float parabola(float a, float b, float c, float x)
{
    float y;
    y = a * x * x + b * x + c;
    return y;
}