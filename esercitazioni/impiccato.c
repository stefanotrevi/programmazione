#include <stdio.h>
#include <stdlib.h>
#define DIM 30

char maiusc(char l);
void intro(void);
void getParola(char parola[DIM], int *dimP, char lettere[DIM]);
void getLettera(int dimP, char lettere[DIM], char parola[DIM], int *tentativi);
void win(int tentativi);

int main(void)
{
    int dimP = 0, tentativi = 0, diverso = 1;
    char parola[DIM], lettere[DIM], quit;

    intro();
    getParola(parola, &dimP, lettere);
    getLettera(dimP, lettere, parola, &tentativi);
    win(tentativi);

    return 0;
}

void intro(void)
{
    //solo per windows CMD
    system("chcp 65001");
    //solo per Windows Powershell
    system("[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8");
    system("cls");
}

void getParola(char parola[DIM], int *dimP, char lettere[DIM])
{
    int i;
    printf("Inserire parola da indovinare:\n");
    scanf("%s", parola);

    for (i = 0; i < DIM && parola[i] != '\0'; i++)
    {
        *dimP = *dimP + 1;
        lettere[i] = '_';
    }

    system("cls");
}

char maiusc(char l)
{
    char lup;
    if (96 < l && l < 123)
        lup = l - 32;
    else
        lup = l;
    return lup;
}

void getLettera(int dimP, char lettere[DIM], char parola[DIM], int *tentativi)
{
    int i, j, trovato = 0, nL = 0, diverso;
    char let;

    printf("\nPronti ad indovinare?\n");
    do
    {
        trovato = 0;
        diverso = 1;
        printf("Inserire una lettera:\n");
        scanf(" %c", &let);
        for (i = 0; i < dimP; i++)
            if (lettere[i] == let)
                diverso = 0;
        for (i = 0; i < dimP; i++)
        {
            if (parola[i] == let && diverso)
            {
                trovato++;
                lettere[i] = let;
            }
            else if (parola[i] == let)
            {
                trovato++;
                diverso = 0;
            }
        }
        for (j = 0; j < dimP; j++)
            if (lettere[j] == let)
                printf("%c ", maiusc(lettere[j]));
            else
                printf("%c ", lettere[j]);

        if (trovato && diverso)
        {
            if (trovato == 1)
                printf("\nBravo, c'è una \"%c\"!\n", let);
            else
                printf("\nBravo, ci sono %d \"%c\"!\n", trovato, let);
            nL += trovato;
        }
        else if (trovato)
            printf("\nHai già trovato questa lettera, prova con un'altra\n");
        else
        {
            *tentativi = *tentativi + 1;
            printf("\nPurtroppo la lettera \"%c\" non c'è. Errori commessi: %d/5\n", let, *tentativi);
        }
    } while (*tentativi < 5 && nL < dimP);
}

void win(int tentativi)
{
    char quit;
    if (tentativi > 5)
        printf("Hai perso.\n");
    else
        printf("Congratulazioni! Hai vinto!\n");

    printf("premi \"q\" per terminare...\n");
    scanf(" q", &quit);
}