//convertitore di nuemri da base 10 a base n (2-16)
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int getBase(void);
int getNumero(char *input, FILE *fT, int base);
int conv10(char *input, FILE *fT, int inSize, int base);

int main(void)
{
    int resto, n = 0, base, i, size = 1, ntemp, inSize, intbuff;
    char buff, resto10, *stampa, input[100];
    FILE *fT;

    fT = fopen("temp.txt", "wt");
    base = getBase();
    inSize = getNumero(input, fT, base);
    conv10(input, fT, inSize, base);
    ntemp = n;
    base = getBase();
    while (ntemp / base)
    {
        size++;
        ntemp /= base;
    }
    stampa = (char *)malloc(size * sizeof(char));

    for (i = size - 1; i >= 0; i--)
    {
        resto = n % base;
        n /= base;
        resto10 = 'a';
        if (resto <= 9)
            stampa[i] = resto;
        else
        {
            resto -= 10;
            resto10 += resto;
            stampa[i] = resto10;
        }
    }

    printf("Risultato:\n");
    for (i = 0; i < size; i++)
    {
        if (stampa[i] < 10)
            printf("%d", stampa[i]);
        else
            printf("%c", stampa[i]);
    }

    printf("\n");
    free(stampa);
    printf("Premere un tasto per uscire...\n");
    scanf("%c", &buff);
    return 0;
}

int getBase(void)
{
    int base;
    printf("Inserire base (2-16):\n");
    do
    {
        scanf("%d", &base);
        if (base < 2 || base > 16)
            printf("Base inserita non valida, riprova:\n");
        fflush(stdin);
    } while (base < 2 || base > 16);
    return base;
}

int getNumero(char *input, FILE *fT, int base)
{
    int i, flag = 1, inSize;
    char buff;

    printf("Inserire numero in base %d:\n", base);
    fflush(stdin);
    while (flag)
    {
        for (i = 0; i < 100 && buff != '\n'; i++)
        {
            scanf("%c", &buff);
            if ((buff <= '9' && buff > 47 + base) || (buff > 86 + base))
                break;
            if (buff != '\n')
                input[i] = buff;
            else
                flag = 0;
        }
        if (flag)
            printf("Almeno una delle cifre fuori dai limiti della base, riprova\n");
        fflush(stdout);
        fflush(stdin);
    }
    i -= 2;
    inSize = i;

    while (i + 1)
    {
        fprintf(fT, "%c ", input[i]);
        i--;
    }
    fclose(fT);
    fT = fopen("temp.txt", "rt");
    if (fT == NULL)
        exit(-2);
    return inSize;
}

int conv10(char *input, FILE *fT, int inSize, int base)
{
    int i, intbuff, n;
    char buff;
    for (i = 0; i < inSize + 1 && !feof(fT); i++)
    {
        if (input[inSize - i] <= '9')
        {
            fscanf(fT, "%d", &intbuff);
            n += intbuff * pow(base, i);
        }
        else
        {
            fscanf(fT, "%c", &buff);
            while (buff == ' ')
                fscanf(fT, "%c", &buff);
            buff -= 87;
            n += buff * pow(base, i);
        }
    }
    printf("%d\n", n);
    return n;
}