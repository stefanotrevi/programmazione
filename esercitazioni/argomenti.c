#include <stdio.h>

int main(int argc, char *argv[])
{
    int i;
    char buff;
    FILE *ft, *fBuff;

    ft = fopen("test.txt", "wt");

    for (i = 1; i < argc; i++)
    {
        fBuff = fopen(argv[i], "rt");
        while (!feof(fBuff))
        {

            fscanf(fBuff, "%c", &buff);
            if (!feof(fBuff))
                fprintf(ft, "%c", buff);
        }
        fprintf(ft, "\n\n");
    }
    fclose(fBuff);
    fclose(ft);

    return 0;
}