//scansiona tre numeri e verifica se il triangolo corrispondente esiste, è degenere/scaleno/isoscele/equilatero
#include <stdio.h>

int main(void)
{
    int a, b, c;

    printf("inserire misura dei tre lati: ");
    scanf("%d%d%d", &a, &b, &c);

    if (a == 0 || b == 0 || c == 0)
        printf("Il triangolo è degenere!\n");
    else if (a + b < c || a - b > c || a + c < b || a - c > b || b + c < a || b - c > a)
        printf("Il triangolo non si può costruire!\n");
    else if (a == b && b == c)
        printf("Il triangolo è equilatero.\n");
    else if (a == b || b == c || a == c)
        printf("Il triangolo è isoscele.\n");

    else
        printf("Il triangolo è scaleno.\n");
}