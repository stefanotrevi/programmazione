#include <stdio.h>

int getNumber(int *n);
int sommaRicorsiva(int n);

int main(void)
{
    int n;

    getNumber(&n);
    printf("%d\n", sommaRicorsiva(n));
    return 0;
}

int getNumber(int *n)
{
    printf("Inserire numero naturale:\n");
    do
    {
        scanf("%d", n);
        if (*n < 0)
            printf("Numero non valido, inserire un numero naturale (incluso 0):\n");
    } while (*n < 0);
}
int sommaRicorsiva(int n)
{
    int somma;
    if (n == 0)
        somma = 0;
    else
        somma = n + sommaRicorsiva(n - 1);
    return somma;
}