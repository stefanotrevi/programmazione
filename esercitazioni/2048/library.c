#include "library.h"

void gioco(void)
{
    unsigned int **campo, dim;
    bool end;

    campo = initCampo(&dim);
    do
    {
        drawCampo(campo, dim);
    } while (editCampo(campo, dim));

    free(campo);
}

unsigned int **initCampo(int *dim)
{
    unsigned int **campo;

    do
    {
        printf("Scegliere dimensione campo (da 1 a 8):\n");
        scanf("%u", dim);
        if (!(*dim) || *dim > 8)
            printf("Dimensione non valida, riprovare (min 1, max 8)...\n");
    } while (!(*dim) || *dim > 8);

    uIntmMalloc(&campo, dim, dim);
    return campo;
}

bool editCampo(unsigned int **campo, unsigned int dim)
{
    char in;

    do
    {
        scanf("%c", &in);
    } while (in != 'w' && in != 'a' && in != 's' && in != 'd');

    switch (in)
    {
    case 'w':

        break;
    case 'a':
        break;
    case 's':
        break;
    case 'd':
        break;
    default:
        break;
    }
}

void drawCampo(unsigned int **campo, unsigned int dim)
{
    int i, j;

    for (i = 0; i < dim; i++)
    {
        for (j = 0; j < dim; j++)
            printf("%u\t", &campo[i][j]);
        printf("\n");
    }
}