//esercizio per testare il debugger
#include <stdio.h>

int main(void)
{
    int dividendo, divisore, quoziente, resto;
    dividendo = 22;
    divisore = 7;
    quoziente = dividendo / divisore;
    resto = dividendo % divisore;
    printf("Quoz.: %d\n", quoziente);
    printf("Resto: %d\n", resto);
}