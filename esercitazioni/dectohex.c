#include <stdio.h>

int intro(void);
void hexToDec();
void decToHex();
char quit(void);

int main(void)
{

    int select;
start:
    select = intro();
    if (select == 1)
        hexToDec();
    else if (select == 2)
        decToHex();
    if (quit() == 'r')
        goto start;
    return 0;
}

int intro(void)
{
    int select;
    printf("Scegliere se convertire hex a dec (1) o dec a hex (2):\n");
    do
    {
        scanf("%d", &select);
        if (select != 1 && select != 2)
            printf("Numero inserito non valido, riprova!\n");
    } while (select != 1 && select != 2);
    return select;
}

void hexToDec()
{
    int r, g, b;
    printf("Inserire valore esadecimale, senza cancelletto:\n");
    scanf("%2x%2x%2x", &r, &g, &b);
    printf("R = %d, G = %d, B = %d\n", r, g, b);
}
void decToHex()
{
    int r, g, b;
    printf("Inserisci valori decimali:\n");
    scanf("%d %d %d", &r, &g, &b);
    printf("Hex: #%02x%02x%02x\n", r, g, b);
}

char quit(void)
{
    char q;
    printf("Premere \'r\' per ricominciare, un altro tasto per uscire...\n");
    fflush(stdin);
    fflush(stdout);
    scanf("%c", &q);
    return q;
}