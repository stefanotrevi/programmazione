#include <stdio.h>
#include "./librerie/matrixlib.h"
#include "./librerie/commonlib.h"
#include "./librerie/userlib.h"
#include "./librerie/listlib.h"

int main(void)
{
    //Aggiungere nuove matrici qui (es. A, B, C, D...;)
    Matrice A, B, C;
    //Aggiungere nuovi interi qui (es a, b, c, d...;)
    int a;
    //Aggiungere nuovi decimali qui (es d1, d2, d3...;)
    float d1;
    //NON MODIFICARE
    FILE *fT;

    //NON MODIFICARE!
    intro();
    fT = openFile(fT, "matrice.txt", "rt");

    /*MODIFICARE DA QUI!
    Nelle funzioni che richiedono "*[qualcosa]" (un puntatore), in generale bisognerà inserire "&[qualcosa]" (un indirizzo),
    a meno che [qualcosa] non sia già stato inizializzato come puntatore (ad esempio un file, o un vettore), allora basterà inserire [qualcosa].
    Nelle funzioni che richiedono "[qualcosa]", inserire "[qualcosa]".
    Se le funzioni restituiscono qualcosa (ad esempio il determinante), ricordarsi di assegnare il risultato ad una variabile, es. d1 = det(M),
    oppure di usare la funzione come se fosse una variabile, es. prodotto(sottoMatrice(A, 0, 0), B) */

    //è consigliabile inizializzare le matrici, vuote o piene (initZeroMatrix o initMatrix), ma non è necessario
    initZeroMatrix(&A);
    
    //Le matrici si possono riempire dalla console (getMatrix) o da un file (getFileMatrix)
    getFileMatrix(fT, &A);
    getFileMatrix(fT, &B);

    /*è consigliabile stampare una matrice ogni volta che viene riempita, per controllare l'esito dell'operazione;
    ovviamente se si ha a che fare con matrici di grandezza notevole, la stampa richiederà un po' di tempo, e potrebbe risultare del tutto superflua*/
    stampaMatrice(A);
    stampaMatrice(B);
    C = prodotto(A, B);
    stampaMatrice(C);

    /*Per stampare un intero (%d), un decimale (%f), o una stringa (%s),
    usare printf("Testo di prova... %d %f %s\n", intero, float, stringa), dove "\n" significa [INVIO] */
    printf("Rango di %s = %d \n", C.nome, rango(C));
    
    //NON MODIFICARE
    quit();
    return 0;
}