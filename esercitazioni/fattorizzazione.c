//scansiona un numero e ne stampa la fattorizzazione
#include <stdio.h>
#include <math.h>
#include <time.h>

int main(void)
{
    int a, b = 2, i, c, j, f, eT;
    clock_t start, end, elap;

    printf("Inserire numero da fattorizzare:\n");
    scanf("%d", &a);
    printf("La fattorizzazione è:\n");
    start = clock();
    while (b <= sqrt(a))
    {
        i = 0;
        c = a;
        j = 2;
        f = 1;

        while (j < b)
        {
            if (b % j != 0)
            {
                j++;
                j += 2;
            }
            else
            {
                f = 0;
                break;
            }
        }

        while (!(c % b) && f)
        {
            c = c / b;
            i++;
        }

        if (i != 0)
        {
            printf("%d^%d", b, i);
            printf(" x ");
        }

        a = a / pow(b, i);
        b++;
    }
    if (a != 1)
        printf("%d^1\n", a);
    end = clock();
    elap = end - start;
    eT = elap * 1000 / CLOCKS_PER_SEC;
    printf("Tempo: %d", eT);
    //printf("\b \b");
}