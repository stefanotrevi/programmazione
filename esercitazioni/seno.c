#include <stdio.h>
#include <math.h>

int main(void)
{
    FILE *fT;
    double n = 0, step = 0.01, max = 6.28;
    int i, range = max / step;

    fT = fopen("table.txt", "wt");
    for (i = 0; i < range; i++)
    {
        fprintf(fT, "%f, ", sin(n));
        n += step;
    }

    fclose(fT);
    return 0;
}