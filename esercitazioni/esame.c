//scansiona una lista di studenti (nome, cognome, matricola e voto), stampando il nome degli studenti che hanno superato l'esame
//più alcune statistiche (quanti studenti hanno preso un determinato voto)
#include <stdio.h>
#define DIM 1000
#define Nvoti 32

typedef struct
{
    char nome[30];
    char cognome[30];
    int matricola;
    int voto;
} stud;

int main(void)
{
    int i, j, n, listaVoti[Nvoti], listaSpV[DIM];
    stud listaStudenti[DIM];

    for (i = 0; i < Nvoti; i++)
        listaVoti[i] = i;

    printf("Inserire numero studenti:\n");
    scanf("%d", &n);
    printf("Inserire elenco studenti (nome cognome matricola voto):\n");

    for (i = 0; i < n && i < DIM; i++)
        scanf("%s %s %d %d", listaStudenti[i].nome, listaStudenti[i].cognome, &listaStudenti[i].matricola, &listaStudenti[i].voto);

    printf("Gli studenti che hanno passato l'esame sono:\n");

    for (i = 0; i < n && i < DIM; i++)
        if (listaStudenti[i].voto >= 18)
            printf("%s %s\n", listaStudenti[i].nome, listaStudenti[i].cognome);

    printf("----- STATISTICHE -----\n");

    for (j = 0; j < Nvoti; j++)
        for (i = 0; i < n && i < DIM; i++)
            if (listaStudenti[i].voto == listaVoti[j])
                listaSpV[j]++;

    printf("Voto - Studenti\n");
    for (i = 0; i < Nvoti; i++)
        printf("%d %d\n", listaVoti[i], listaSpV[i]);

    return 0;
}

/* LISTA "STUDENTI":
Giorgio Spaggiari 1 13
Alfredo Gogini 2 19
Gianni Morandi 3 23
Ipse Dixit 4 12
John Lennon 5 0
Alberto Luciani 6 18
Marco Rossi 7 18
Sigmondo Lorena 8 30
Bobby Solo 9 31
Alberto Angela 10 31
Piero Angela 11 31
Bruno Vespa 12 10
Simona Ventura 13 10
Michael Schumacker 14 18
Pino Pellegrino 15 22
Maurizio Crozza 16 22
Maurizio Costanzo 17 12
Antonella Clerici 18 11
Homer Simpson 19 1
Timmy Turner 20 1
Filippo Secondo 21 25
Giorgio Quinto 22 21
Robert DeNiro 23 28
Tony Stark 24 31
Peter Parker 25 30
Johnny English 26 2
Luke Skywalker 27 27
Mace Windu 28 23
Samuel Jackson 29 31
Oliver Twist 30 14
Giuseppe Garibaldi 31 5
Aulo Gellio 32 31
Marco Cicerone 33 29
Caio Sempronio 34 14
Catone Maior 35 23
Catone Minor 36 12
Giulio Cesare 37 21
Ottaviano Augusto 38 27


*/