//calcola i giorni di vita di una persona a partire dalla sua data di nascita, usando il calcolo del giorno giuliano
#include <stdio.h>

int main(void)
{
    //initialize main variables
    int JD, JD2, N0, N1, N2, N3, G, M, A;
    //initialize and assign constants
    int C0 = 32075, C1 = 14, C2 = 1461, C3 = 4800, C4 = 367;
    int C5 = 2, C6 = 12, C7 = 3, C8 = 4900, C9 = 400, C10 = 4;

    printf("Inserire giorno, mese ed anno attuali: ");
    scanf("%d%d%d", &G, &M, &A);

    //calculating JD1
    N0 = (M - C1) / C6;
    N1 = C2 * (A + C3 + N0) / C10;
    N2 = C4 * (M - C5 - C6 * N0) / C6;
    N3 = C7 * (A + C8 + N0) / C9;
    JD = N1 + N2 - N3 + G - C0;

    printf("Inserire data di nascita: ");
    scanf("%d%d%d", &G, &M, &A);

    //calculating JD2
    N0 = (M - C1) / C6;
    N1 = C2 * (A + C3 + N0) / C10;
    N2 = C4 * (M - C5 - C6 * N0) / C6;
    N3 = C7 * (A + C8 + N0) / C9;
    JD2 = N1 + N2 - N3 + G - C0;

    printf("Hai %d giorni, buon complegiorno!\n", JD - JD2);
}