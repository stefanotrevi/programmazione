//controlla che i file con nome in *argv[] esistano, eventualmente stampando il primo che non esiste
void eccezioneFile(FILE *f, char *nomefile);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int *argc, int argHp);
