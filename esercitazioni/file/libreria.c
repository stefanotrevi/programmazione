#include <stdio.h>
#include <stdlib.h>
#include "libreria.h"

void eccezioneFile(FILE *f, char *nomefile)
{
    if (f == NULL)
    {
        printf("Il file \"%s\" non esiste.\n", nomefile);
        exit(-2);
    }
}

void eccezioneArg(int *argc, int argHp)
{
    if (*argc < argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
    *argc = argHp;
}