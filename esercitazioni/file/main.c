#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "libreria.h"

int main(int argc, char **argv)
{
    FILE *f, *f1;
    int a[4], i, som = 0, somf;

    eccezioneArg(&argc, 3);
    f = fopen(argv[1], "wt");
    eccezioneFile(f, argv[1]);
    printf("Inserire 4 numeri per sapere la somma\n");
    for (i = 0; i < 4; i++)
    {
        scanf("%d", &a[i]);
        som += a[i];
    }
    fprintf(f, "%d", som);
    fclose(f);
    f = fopen("cometipare.txt", "rt");
    eccezioneFile(f, "cometipare.txt");
    fscanf(f, "%d", &somf);
    fclose(f);
    f1 = fopen(argv[2], "wb");
    eccezioneFile(f1, argv[2]);
    fwrite(&somf, sizeof(somf), 1, f1);
    rewind(f1);
    fread(&somf, sizeof(somf), 1, f1);
    fclose(f1);
    printf("%d\n", somf);
    return 0;
}