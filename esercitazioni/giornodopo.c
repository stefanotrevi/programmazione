//scansiona una data e ne stampa il giorno successivo, incluso il giorno della settimana. Un misto fra "gg" e "giornimese"
#include <stdio.h>

typedef struct
{
    int g, m, a;
} data;

typedef struct
{
    char lg1, lg2, lg3, lg4, lg5, lg6, lg7, lg8, lg9;
} giorno;

int cgg(int g, int m, int a)
{
    int gg, n0, n1, n2, n3, C0 = 32075, C1 = 14, C2 = 1461, C3 = 4800, C4 = 367, C5 = 2, C6 = 12, C7 = 3, C8 = 4900, C9 = 400, C10 = 4;
    n0 = (m - C1) / C6;
    n1 = C2 * (a + C3 + n0) / C10;
    n2 = C4 * (m - C5 - C6 * n0) / C6;
    n3 = C7 * (a + C8 + n0) / C9;
    gg = n1 + n2 - n3 + g - C0;
    return gg;
}

int bisestile(int a)
{
    int ds1 = 4, ds2 = 100, ds3 = 400, val1, val2, val3, val = 0;
    val1 = a % ds1;
    val2 = a % ds2;
    val3 = a % ds3;
    if (val1 == 0 && val2 != 0 || val3 == 0)
        val = 1;
    return val;
}

int exception(int g, int m, int a, int val)
{
    int flag = 1;

    if (g < 0 || g > 31)
        printf("Il numero di giorni inserito non è valido, riprova!\n");
    else if (m > 12 || m < 1)
        printf("Il mese inserito non esiste, riprova!\n");
    else if (g > 30 && (m == 11 || m == 4 || m == 6 || m == 9 || m == 2))
        printf("Il mese inserito ha meno di 31 giorni, riprova!\n");
    else if (g == 30 && m == 2)
        printf("Febbraio non ha 30 giorni!\n");
    else if (g == 29 && m == 2 && val == 0)
        printf("L'anno scelto non è bisestile, febbraio ha 28 giorni, riprova!\n");
    else
        flag = 0;
    return flag;
}

data calcoli(int g, int m, int a, int val)
{
    int g0 = 1, m0 = 1;
    data d;

    if (g < 30 && (m == 11 || m == 4 || m == 6 || m == 9))
        g++, m, a;
    else if (g == 28 && m == 2)
    {
        if (val)
            g++, m, a;
        else
            g = g0, m++, a;
    }
    else if (g == 29 && m == 2)
        g = g0, m++, a;
    else if (g < 28 && m == 2)
        g++, m, a;
    else if (g == 30 && (m == 11 || m == 4 || m == 6 || m == 9))
        g = g0, m++, a;
    else if (g == 31 && m == 12)
        g = g0, m = m0, a++;
    else if (g == 31)
        g = g0, m++, a;
    else if (g < 31)
        g++, m, a;

    d.g = g;
    d.m = m;
    d.a = a;
    return d;
}

giorno giornodopo(int gg)
{
    giorno gs;
    switch (gg % 7)
    {
    case 6:
        gs.lg1 = 'L';
        gs.lg2 = 'u';
        gs.lg3 = 'n';
        gs.lg4 = 'e';
        gs.lg5 = 'd';
        gs.lg6 = 'i';
        gs.lg7 = 0;
        gs.lg8 = 0;
        gs.lg9 = 0;
        break;
    case 0:
        gs.lg1 = 'M';
        gs.lg2 = 'a';
        gs.lg3 = 'r';
        gs.lg4 = 't';
        gs.lg5 = 'e';
        gs.lg6 = 'd';
        gs.lg7 = 'i';
        gs.lg8 = 0;
        gs.lg9 = 0;
        break;
    case 1:
        gs.lg1 = 'M';
        gs.lg2 = 'e';
        gs.lg3 = 'r';
        gs.lg4 = 'c';
        gs.lg5 = 'o';
        gs.lg6 = 'l';
        gs.lg7 = 'e';
        gs.lg8 = 'd';
        gs.lg9 = 'i';
        break;
    case 2:
        gs.lg1 = 'G';
        gs.lg2 = 'i';
        gs.lg3 = 'o';
        gs.lg4 = 'v';
        gs.lg5 = 'e';
        gs.lg6 = 'd';
        gs.lg7 = 'i';
        gs.lg8 = 0;
        gs.lg9 = 0;
        break;
    case 3:
        gs.lg1 = 'V';
        gs.lg2 = 'e';
        gs.lg3 = 'n';
        gs.lg4 = 'e';
        gs.lg5 = 'r';
        gs.lg6 = 'd';
        gs.lg7 = 'i';
        gs.lg8 = 0;
        gs.lg9 = 0;
        break;
    case 4:
        gs.lg1 = 'S';
        gs.lg2 = 'a';
        gs.lg3 = 'b';
        gs.lg4 = 'a';
        gs.lg5 = 't';
        gs.lg6 = 'o';
        gs.lg7 = 0;
        gs.lg8 = 0;
        gs.lg9 = 0;
        break;
    default:
        gs.lg1 = 'D';
        gs.lg2 = 'o';
        gs.lg3 = 'm';
        gs.lg4 = 'e';
        gs.lg5 = 'n';
        gs.lg6 = 'i';
        gs.lg7 = 'c';
        gs.lg8 = 'a';
        gs.lg9 = 0;
        break;
    }
    return gs;
}
int main(void)
{

    int g, m, a, gg, val, flag;
    data d;
    giorno gs;

    do
    {
        printf("Inserire giorno, mese ed anno: ");
        scanf("%d%d%d", &g, &m, &a);

        flag = exception(g, m, a, val);
    } while (flag);

    gg = cgg(g, m, a);
    val = bisestile(a);
    d = calcoli(g, m, a, val);
    gs = giornodopo(gg);

    printf("Il giorno successivo è %c%c%c%c%c%c%c%c%c %d/%d/%d\n", gs.lg1, gs.lg2, gs.lg3, gs.lg4, gs.lg5, gs.lg6, gs.lg7, gs.lg8, gs.lg9, d.g, d.m, d.a);
}