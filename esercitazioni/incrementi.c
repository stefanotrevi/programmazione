//scansioan un numero ed esegue alcune operazioni in forma compatta su di esso
#include <stdio.h>

int main(void)
{
    int a;
    printf("Inserire valore di a: ");
    scanf("%d", &a);
    a += 2;
    printf("%d\n", a);
    a -= 2;
    a *= 2;
    printf("%d\n", a);
    a /= 2;
    a *= a;
    printf("%d\n", a);
}