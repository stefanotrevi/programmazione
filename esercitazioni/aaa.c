#include <stdio.h>
#include <math.h>
int main()
{
    long long int num = 1, t;

    while (num >= 0)
        ++num;
    --num;
    t = (long long int)(log10((double)num + 1.0) / log10(2.0));
    printf("\n max intero rappresentabile = %ld\t cifre = %ld", num, t + 1);
    num = -1;
    while (num < 0)
        --num;
    ++num;
    printf("\n min intero rappresentabile = %ld \n\n", num);
}