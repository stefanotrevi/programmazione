//scansiona due numeri interi e ne stampa il rapporto
#include <stdio.h>

int main(void)
{
    int dd;
    int dv;
    printf("Inserire dividendo: ");
    scanf("%d", &dd);
    printf("\nInserire divisore: ");
    scanf("%d", &dv);
    printf("\nRisultato: %d\n", dd / dv);
}