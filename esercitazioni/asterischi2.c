//stessa cosa di asterischi, ma con una funzione esterna, e stampa a righe
#include <stdio.h>

int star()
{
    int i;
    for (i = 0; i < 20; i++)
        printf("*");
}

int main(void)
{
    int i;
    for (i = 0; i < 5; i++)
    {
        star();
        printf("\n");
    }
}