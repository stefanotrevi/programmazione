#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    FILE *f;
    int i;
    char name[30], tin;
    printf("Inserisci il nome di un file da aprire:\n");
    scanf("%s", name);
    f = fopen(name, "rt");
    if (f == NULL)
    {
        printf("File non trovato\n");
        exit(-1);
    }
    
    while (!feof(f))
    {
        fscanf(f, "%c", &tin);
        printf("%c", tin);
    }
    fclose(f);
    return 0;
}