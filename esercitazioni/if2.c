//scansiona un numero e stampa se è positivo, negativo o nullo; pari o dispari
#include <stdio.h>

int main(void)
{
    int n;
    printf("Inserire numero da valutare: ");
    scanf("%d", &n);
    if (n > 0)
        printf("Il numero è positivo, ");
    else if (n == 0)
        printf("Il numero è nullo, ");
    else
        printf("Il numero è negativo, ");
    if (n % 2 == 0)
        printf("pari\n");
    else
        printf("dispari\n");
}