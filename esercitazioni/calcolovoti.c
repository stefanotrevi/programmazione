#include <stdio.h>
#define MAXV 100

int main(void)
{
    int voti[MAXV], pesi[MAXV], i, buff = 1, nV, nP, res = 0, totP = 0, vL;
    float voto;
    printf("Inserire lista voti:\n");

    for (i = 0; i < MAXV && buff; i++)
    {
        scanf("%d", &buff);
        if (buff)
            voti[i] = buff;
    }
    nV = i;
    buff = 1;
    printf("Inserire, nello stesso ordine, lista pesi (CFU):\n");
    for (i = 0; i < MAXV && buff; i++)
    {
        scanf("%d", &buff);
        if (buff)
        {
            pesi[i] = buff;
            totP += buff;
        }
    }
    nP = i;

    printf("Inserire voto tesi:\n");
    scanf("%d", &vL);

    for (i = 0; i < nP && i < nV; i++)
        res = res + voti[i] * pesi[i];
    voto = res / totP * 11 / 3 + 1 + vL;
    printf("Voto finale, in centodecimi: %f\n", voto);
    return 0;
}