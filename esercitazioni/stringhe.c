//scansiona una stringa e ne stampa il numero di caratteri
#include <stdio.h>
#define DIM 10

int main(void)
{
    char i;
    char s[DIM - 1];
    scanf("%s", s);
    for (i = 0; s[i] != '\0' && i < DIM; i++)
        ;
    printf("%d\n", i);
    return 0;
}