//scansiona due date e calcola la più vecchia
#include <stdio.h>

struct data
{
    int g;
    int m;
    int a;
} d1, d2;

int bisestile(int a);
int exception(int g, int m, int a, int val);

int main(void)
{
    int flag1, flag2, val1, val2;
    printf("Inserire due date:\n");

    do
    {
        scanf("%d%d%d%d%d%d", &d1.g, &d1.m, &d1.a, &d2.g, &d2.m, &d2.a);
        val1 = bisestile(d1.a);
        val2 = bisestile(d2.a);
        flag1 = exception(d1.g, d1.m, d1.a, val1);
        flag2 = exception(d2.g, d2.m, d2.a, val2);
    } while (flag1 || flag2);

    if ((d1.a < d2.a) || (d1.a == d2.a && d1.m < d2.m) || (d1.a == d2.a && d1.m == d2.m && d1.g < d2.g))
        printf("la prima data precede la seconda\n");
    else if (d1.g == d2.g)
        printf("Le due date sono uguali\n");
    else
        printf("la seconda data precede la prima\n");
    return 0;
}

int bisestile(int a)
{
    int ds1 = 4, ds2 = 100, ds3 = 400, val1, val2, val3, val = 0;
    val1 = a % ds1;
    val2 = a % ds2;
    val3 = a % ds3;
    if (val1 == 0 && val2 != 0 || val3 == 0)
        val = 1;
    return val;
}

int exception(int g, int m, int a, int val)
{
    int flag = 1;

    if (g < 0 || g > 31)
        printf("Il numero di giorni inserito non è valido, riprova!\n");
    else if (m > 12 || m < 1)
        printf("Il mese inserito non esiste, riprova!\n");
    else if (g > 30 && (m == 11 || m == 4 || m == 6 || m == 9 || m == 2))
        printf("Il mese inserito ha meno di 31 giorni, riprova!\n");
    else if (g == 30 && m == 2)
        printf("Febbraio non ha 30 giorni!\n");
    else if (g == 29 && m == 2 && val == 0)
        printf("L'anno scelto non è bisestile, febbraio ha 28 giorni, riprova!\n");
    else
        flag = 0;
    return flag;
}