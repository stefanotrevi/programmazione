//simile ad incrementi1, ma con un numero fissato
#include <stdio.h>

int main(void)
{
    int a = 5;
    printf("%d\n", a);
    a += a + 6;
    printf("%d\n", a);
    a = 5;
    a += a = 4;
    printf("%d\n", a);
    a = 5;
    a += a++;
    printf("%d\n", a);
    a = 5;
    a = a + a++;
    printf("%d\n", a);
    a = 5;
}