// scansiona il prezzo da pagare e lo divide nelle rispettive banconote e monete
#include <stdio.h>

int main(void)
{
	int carrello;
	int prz;
	int D = 500;
	int CC = 200;
	int C = 100;
	int L = 50;
	int XX = 20;
	int X = 10;
	int V = 5;
	int EE = 2;
	int E = 1;

	printf("inserire prezzo totale del carrello: ");
	scanf("%d", &carrello);
	printf("\nBisogna pagare: ");
	prz = carrello / D;
	carrello = carrello - prz * D;
	if (prz != 0 && prz != 1)
		printf("%d banconote da €%d", prz, D);
	else if (prz == 1){
		printf(" %d banconota da €%d", prz, D);
	}

	prz = carrello / CC;
	carrello = carrello - prz * CC;
	if (prz != 0 && prz != 1)
		printf(" %d banconote da €%d", prz, CC);
	else if (prz == 1){
		printf(" %d banconota da €%d", prz, CC);
	}
	prz = carrello / C;
	carrello = carrello - prz * C;
	if (prz != 0 && prz != 1)
		printf(" %d banconote da €%d", prz, C);
	else if (prz == 1){
		printf(" %d banconota da €%d", prz, C);
	}
	prz = carrello / L;
	carrello = carrello - prz * L;
	if (prz != 0 && prz != 1)
		printf(" %d banconote da €%d", prz, L);
	else if (prz == 1)
		printf(" %d banconota da €%d", prz, L);

	prz = carrello / XX;
	carrello = carrello - prz * XX;
	if (prz != 0 && prz != 1)
		printf(" %d banconote da €%d", prz, XX);
	else if (prz == 1)
		printf(" %d banconota da €%d", prz, XX);

	prz = carrello / X;
	carrello = carrello - prz * X;
	if (prz != 0 && prz != 1)
		printf(" %d banconote da €%d", prz, X);
	else if (prz == 1)
		printf(" %d banconota da €%d", prz, X);

	prz = carrello / V;
	carrello = carrello - prz * V;
	if (prz != 0 && prz != 1)
		printf(" %d banconote da €%d", prz, V);
	else if (prz == 1)
		printf(" %d banconota da €%d", prz, V);

	prz = carrello / EE;
	carrello = carrello - prz * EE;
	if (prz != 0 && prz != 1)
		printf(" più %d monete da €%d", prz, EE);
	else if (prz == 1)
		printf(" %d moneta da €%d", prz, EE);
		else printf(" ");

	prz = carrello / E;
	carrello = carrello - prz * E;
	if (prz != 0 && prz != 1)
		printf(" %d monete da €%d", prz, E);
	else if (prz == 1)
		printf(" %d moneta da €%d", prz, E);
		else printf(" ");
		printf("\n");
}
