//scansiona e stampa array
#include <stdio.h>
#define D 10

int main(void)
{
    int v[D], i, n0;

    printf("Inserire numero:\n");
    scanf("%d", &n0);
    printf("Inserire 10 numeri:\n");

    for (i = 0; i < D; i++)
    {
        scanf("%d", &v[i]);
        v[i] = v[i] + n0;
    }
    printf("-----------------\n");
    for (i = 0; i < D; i++)
        printf("%d\n", v[i]);

    return 0;
}