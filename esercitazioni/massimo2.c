//come "massimo", ma con 3 numeri
#include <stdio.h>

int max(int a, int b, int c)
{
    if (a > b && a > c)
        return a;
    else if (b > a && b > c)
        return b;
    else
        return c;
}

int main(void)
{
    int a, b, c;

    printf("Inserire 3 numeri:\n");
    scanf("%d%d%d", &a, &b, &c);
    if (a == b && b == c)
        printf("I tre numeri sono uguali\n");
    else
        printf("%d\n", max(a, b, c));
}