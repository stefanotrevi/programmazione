#include <stdio.h>

typedef struct
{
    int r;
    int im;
    char i;
} intCplx;

intCplx cplxProduct(intCplx a, intCplx b);
intCplx readCplx(intCplx a);
int printCplx(intCplx a);

int main(void)
{
    intCplx result, temp;
    int i, j;

    printf("Inserire numero di elementi da sommare:\n");
    scanf("%d", &j);

    intCplx a[j];

    for (i = 0; i < j; i++)
    {
        printf("Inserire un numero complesso:\n");
        readCplx(a[j]);
    }

    result = a[0];

    for (i = 0; i < j - 1; i++)
    {
        result = cplxProduct(result, a[i + 1]);
    }

    printf("Il prodotto è: ");
    printCplx(result);

    return 0;
}

intCplx cplxProduct(intCplx a, intCplx b)
{
    intCplx result;
    int I = -1;
    result.r = a.r * b.r + I * a.im * b.im;
    result.im = a.im * b.r + a.r * b.im;
    result.i = 'i';
    return result;
}

intCplx readCplx(intCplx a)
{
    scanf("%d %d%c", &a.r, &a.im, &a.i);
    return a;
}

int printCplx(intCplx a)
{
    printf("%d %d%c", a.r, a.im, a.i);
    return 0;
}