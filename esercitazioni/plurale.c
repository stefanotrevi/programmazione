//scansiona una parola in "cia" o "gia" e ne stampa il plurale a seconda della lettera che precede l'ultima sillaba
#include <stdio.h>
#define DIM 12

int main(void)
{
    int i;
    char in[DIM];

    printf("Inserire parola che termina in \"gia\" o \"cia\":\n");
    scanf("%s", in);

    for (i = 0; i < DIM - 2; i++)
        if (in[i] == 'g' || in[i] == 'c' && in[i + 1] == 'i' && in[i + 2] == 'a')
            if (in[i - 1] == 'a' || in[i - 1] == 'e' || in[i - 1] == 'i' || in[i - 1] == 'o' || in[i - 1] == 'u')
                in[i + 2] = 'e';
            else
            {
                in[i + 1] = 'e';
                in[i + 2] = '\0';
            };
    ;
    printf("%s\n", in);
    return 0;
}