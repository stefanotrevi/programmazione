//converte un testo in maiuscolo
#include <stdio.h>

char maiusc(char l);

int main(void)
{
    char text;
    printf("Inserire del testo:\n");

    do
    {
        scanf("%c", &text);
        printf("%c", maiusc(text));
    } while (text != '\n');

    return 0;
}

char maiusc(char l)
{
    char lup;
    if (96 < l && l < 123)
        lup = l - 32;
    else
        lup = l;
    return lup;
}