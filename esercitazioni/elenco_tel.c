//stampa un elenco telefonico immaginario a seguito di un accesso autorizzato tramite PIN
#include <stdio.h>

int main(void)
{
    int a = 1, b = 2, c = 3, d = 4;
    int pin;
    int num;
    int i = 1;

    do
    {
        printf("Inserire PIN: ");
        scanf("%d", &pin);
        if (pin == 44122)
        {
            printf("Accesso consentito, caricamento contatti...\n");
            printf("%d - Mario Abate: 334 2345 311\n%d - Silvio Berlusconi: 346 3451 780\n%d - Gianni Morandi: 354 1598 065\n%d - Matteo Renzi: 365 2146 967\n", a, b, c, d);
            i = 5;
        }
        else
        {
            i++;
            printf("Riprova. Numero tentativi rimasti: %d\n", 4 - i);
        }
    } while (i <= 3);
    if (i == 4)
        printf("Accesso bloccato! Numero massimo di tentativi superato\n");
    else
    {
        printf("Scegliere contatto da chiamare: ");
        scanf("%d", &num);
        if (num >= 1 && num <= 4)
            printf("Sto chiamando il contatto %d\n", num);
            else printf("Il contatto non esiste.\n");
    }
}