//scansiona tot numeri e li stampa in ordine crescente
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define D 100000

//algoritmo intuitivo ma estremamente lento, non usare! (10.000 elementi in 300 sec.)
void ordinaSwap(int v[D], int dim);
//Implementazione dell'InsertionSort, decisamente meglio del precedente (10.000 elementi in 2 sec.)
void ordinaReplace(int v[D], int dim);
//Tentativo di implementazione del QuickSort (10.000 elementi in circa 2 msec. )
//Purtrtoppo, se eseguito nel debugger funziona perfettamente, una volta avviato da console dà errore (presumibilmente un leaking di memoria...)
void ordinaRicorsivo(int *mainV, int *v, int dim, int *fill);
void printArray(int v[D], int dim);
int getArray(int v[D]);
int checkArray(int *v, int dim);
void calcolaDim(int *v, int dim, int pivot, int average, int *ndim1, int *ndim2);
void riempiPartialArray(int *v, int *v1, int *v2, int dim, int dim2, int pivot, int average);
void riempiMainArray(int *mainV, int *v, int dim, int *fill);

int main(void)
{
    int v[D], dim, fill, elapsedTime;
    clock_t start, end, elapsed;

    dim = getArray(v);
    start = clock();
    //ordinaSwap(v, dim);
    //ordinaReplace(v, dim);
    ordinaRicorsivo(v, v, dim, &fill);
    end = clock();
    elapsed = end - start;
    elapsedTime = elapsed * 1000 / CLOCKS_PER_SEC;
    printArray(v, dim);
    printf("%d", elapsedTime);
}

int getArray(int v[D])
{
    int i, dim;
    FILE *fT;

    printf("Apertura file in corso...\n");
    fT = fopen("ordinamento.txt", "rt");
    if (fT == NULL)
    {
        exit(-1);
    }
    for (i = 0; i < D && !feof(fT); i++)
    {
        if(!feof(fT))
        fscanf(fT, "%d", &v[i]);
    }
    dim = i;
    printf("Inizio ordinamento...\n");
    return dim;
}

void printArray(int v[D], int dim)
{
    int i;
    for (i = 0; i < dim; i++)
        printf("%d ", v[i]);
    printf("\n");
}
void ordinaSwap(int v[D], int dim)
{
    int i, flag = 0, temp;
    for (i = 0; i < dim; i++)
    {
        if (flag)
        {
            i = 0;
            flag = 0;
        }
        if (v[i + 1] < v[i])
        {
            temp = v[i];
            v[i] = v[i + 1];
            v[i + 1] = temp;
            flag = 1;
            i = 0;
        }
    }
}

void ordinaReplace(int v[D], int dim)
{
    int i, j, temp;
    for (i = 0; i < dim; i++)
    {
        j = 0;
        while (j != i && v[i] < v[i - j - 1])
            j++;
        if (j)
        {
            temp = v[i - j];
            v[i - j] = v[i];
            v[i] = temp;
            i--;
        }
    }
}

void ordinaRicorsivo(int *mainV, int *v, int dim, int *fill)
{
    int pivot, *v1, *v2, ndim1, ndim2, average = dim / 2, flag = 1;

    if (checkArray(v, dim))
    {
        riempiMainArray(mainV, v, dim, fill);
    }
    else
    {
        do
        {
            ndim1 = 0;
            pivot = v[average];
            calcolaDim(v, dim, pivot, average, &ndim1, &ndim2);
            if (!ndim2 && average > 0 && flag)
                average--;
            else if (!ndim2)
            {
                if (average == 0)
                {
                    flag = 0;
                    average = dim / 2;
                }
                average++;
            }

        } while (!ndim2);
        v1 = (int *)malloc(ndim1 * sizeof(int));
        v2 = (int *)malloc(ndim2 * sizeof(int));
        riempiPartialArray(v, v1, v2, dim, ndim2, pivot, average);
        ordinaRicorsivo(mainV, v1, ndim1, fill);
        mainV[*fill] = pivot;
        *fill = *fill + 1;
        ordinaRicorsivo(mainV, v2, ndim2, fill);
        free(v1);
        free(v2);
    }
}

int checkArray(int *v, int dim)
{
    int i, flag = 0;
    for (i = 0; i < dim; i++)
        if (v[i] != v[0])
            break;
    if (i == dim)
        flag = 1;
    return flag;
}
void calcolaDim(int *v, int dim, int pivot, int average, int *ndim1, int *ndim2)
{
    int i;

    for (i = 0; i < dim; i++)
        if (v[i] <= pivot && i != average)
            *ndim1 = *ndim1 + 1;
    if (dim - *ndim1 == 1)
        *ndim2 = 0;
    else
        *ndim2 = dim - *ndim1 - 1;
}

void riempiPartialArray(int *v, int *v1, int *v2, int dim, int dim2, int pivot, int average)
{
    int i, j = 0, k = 0;
    if (dim2)
        for (i = 0; i < dim; i++)
            if (v[i] <= pivot && i != average)
            {
                v1[j] = v[i];
                j++;
            }
            else if(i != average)
            {
                v2[k] = v[i];
                k++;
            }
}

void riempiMainArray(int *mainV, int *v, int dim, int *fill)
{
    int i;
    for (i = *fill; i < *fill + dim; i++)
        mainV[i] = v[0];
    *fill = *fill + dim;
}