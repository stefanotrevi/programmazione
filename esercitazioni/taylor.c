//scansiona un numero e ne calcola il seno attraverso lo sviluppo in serie di Taylor.
#include <stdio.h>
#include <math.h>

int factorial(int n)
{
    return (n == 1 || n == 0) ? 1 : factorial(n - 1) * n;
}
int main(void)
{
    double a, sinx = 0;
    int cs, b = 0;

    printf("Inserire numero da valutare:\n");
    scanf("%lf", &a);
    printf("Inserire numero cifre significative:\n");
    scanf("%d", &cs);

    while (fabs(sinx - sin(a)) >= pow(10, -(cs + 1)))
    {
        sinx = sinx + (pow(-1, b) * pow(a, 2 * b + 1)) / factorial(2 * b + 1);
        b++;
    }
    printf("%lf\n", sinx);
    printf("%lf\n", sin(a));
}