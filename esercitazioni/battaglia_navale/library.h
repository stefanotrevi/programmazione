#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../librerie/commonlib.h"

//Costanti globali che possono essere modificate senza dover modificare il programma (basta ricompilare)
#define DIM 10          //dimensione campo da gioco, fra 2 e 26 (maggiore serve implementazione doppia coordinata letterale)
#define NCAS DIM*DIM   //numero delle caselle, DEVE essere uguale al quadrato di 'DIM'
//Costanti globali che possono essere modificate, ma richiedono una modifica di alcune funzioni (initFlotta, setFlotta, IAshoot)
#define SIZE 7        //dimensione della nave più grande
#define FSIZE 12     //dimensione della flotta

#ifdef __unix__
#define CLEAR "clear"
#elif _WIN32
#define CLEAR "cls"
#else
#endif
//Definizione di casella: due coordinate e uno stato
typedef struct
{
    int x;     //Coordinata x
    int y;     //Coordinata y
    int stato; //Stato della casella: libero = 0, occupato = 1, colpito (acqua) = 2, colpito (nave) = 3, colpito (nave affondata) = 4;
} casella;

//Definizione di nave: uno scafo (un vettore di caselle), una dimensione dello scafo e uno stato della nave
typedef struct
{
    casella scafo[SIZE];
    int dim;  //La dimensione è utile per capire la classe della nave, infatti è diversa per ogni tipologia
    int flag; //Stato della nave, booleano (affondata: 1, non affondata: 0)
} nave;

//Funzione da chiamare nel main, incapsula una serie di altre funzioni (intro, initCampo...)
int gioco(void);

//Mostra una breve introduzione e chiede all'utente di selezionare la difficoltà
int intro(void);

//Mostra un messaggio di vittoria/sconfitta, e chiede se uscire o riprovare
int end(int winner);

//Chiede all'utente se vuole posizionare le navi manualmente/a caso
int randCampo(void);

//Genera un campo, se manualmente, mostra un breve tutorial
void genCampo(casella campo[DIM][DIM], nave flotta[FSIZE], int rand);

//Inizializza un campo (ovvero imposta una matrice con valori pari alla loro posizione XY)
void initCampo(casella campo[DIM][DIM]);

//Inizializza
void initFlotta(nave flotta[FSIZE]);
int setFlotta(casella campo[DIM][DIM], nave flotta[FSIZE], int rand);
void randCell(casella *out, casella *allow, int adim);
void placeNave(casella campo[DIM][DIM], nave *ship, casella offset, casella *allow, int *adim, casella cp[NCAS]);
void removeCell(casella *vc, int *vdim, casella n, casella cp[NCAS]);
int freeCell(casella c, casella cp[NCAS], int dim);
int toccano(casella c, casella cp, int angoli);
int coincidono(casella c, casella cp);
void drawCampo(casella campo[DIM][DIM], int show);
int editCampo(casella campo[DIM][DIM], nave flotta[FSIZE], int level);
casella getCasella(int place);
int colpisciNave(casella t, nave flotta[FSIZE]);
int affondaNave(nave *n);
int flottaAffondata(nave flotta[FSIZE]);
void tutorial(void);
char *nomeNave(int n);
casella IAshoot(casella campo[DIM][DIM], int level);
void ruotaNave(nave *ship);