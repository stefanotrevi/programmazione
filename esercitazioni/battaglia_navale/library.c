#include "library.h"

int gioco(void)
{
    casella campo1[DIM][DIM], campo2[DIM][DIM]; //Il campo è una matrice di caselle
    nave flotta1[FSIZE], flotta2[FSIZE];        //la flotta è un array di navi
    int level, endgame, turn = 0;

    level = intro();
    genCampo(campo1, flotta1, randCampo());
    genCampo(campo2, flotta2, 1);

    do
    {
        system(CLEAR);
        drawCampo(campo1, 1);
        drawCampo(campo2, 0);
        //Se è il turno del giocatore
        if (!turn)
        {
            endgame = editCampo(campo2, flotta2, 0);
            turn = 1;
        }
        else
        {
            endgame = editCampo(campo1, flotta1, level);
            turn = 0;
        }
    } while (endgame);
    system(CLEAR);
    drawCampo(campo1, 1);
    drawCampo(campo2, 1);
    return turn;
}

int intro(void)
{
    char start;
    int level;

    //solo per windows CMD
    system("chcp 65001");
    //solo per Windows Powershell
    system("[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8");
    system(CLEAR);
    printf("Benvenuti a battaglia navale! Per iniziare, premere un tasto qualsiasi:\n");
    scanf("%c", &start);
    printf("Selezionare la difficoltà (1 = facile; 2 = medio; 3 = difficile):\n");
    do
    {
        scanf("%d", &level);
        flusher();
        if (level <= 0 || level > 3)
            printf("La difficoltà selezionata non esiste, riprovare...\n");
    } while (level <= 0 || level > 3);

    system(CLEAR);
    return level;
}

int end(int winner)
{
    char c;
    if (winner)
        printf("Congratulazioni, hai vinto!\n");
    else
        printf("Peccato, hai perso...\n");
    printf("Premere \'q\' per uscire, \'r\' per ricominciare:\n");
    do
    {
        scanf("%c", &c);
        if (c != 'q' && c != 'r')
            printf("Per uscire, premere il tasto \'q\', per ricominciare, premere il tasto \'r\'...\n");
    } while (c != 'q' && c != 'r');
    if (c == 'r')
        return 1;
    return 0;
}

int randCampo(void)
{
    int random;

    printf("Vuoi posizionare tu le navi (0) o preferisci un campo generato a caso (1)?\n");
    do
    {
        scanf("%d", &random);
        flusher();
        if (random != 0 && random != 1)
            printf("Opzione non valida. Premere \'0\' per generare il campo manualmente, '\'1\' per generarlo a caso:\n");
    } while (random != 0 && random != 1);
    system(CLEAR);

    return random;
}
void genCampo(casella campo[DIM][DIM], nave flotta[FSIZE], int rand)
{

    srand(time(NULL));
    if (rand)
        //A volte il campo non è generabile casualmente (i posti sono tutti bloccati), quindi bisogna ripetere la procedura
        do
        {
            //Inizializza un campo
            initCampo(campo);
            //Inizializza una flotta
            initFlotta(flotta);
        } while (!setFlotta(campo, flotta, rand));
    else
    {
        //Per la generazione manuale, viene proposto un breve tutorial salvato su file di testo
        tutorial();
        do
        {
            initCampo(campo);
            initFlotta(flotta);
        } while (!setFlotta(campo, flotta, rand));
    }
}

void initCampo(casella campo[DIM][DIM])
{
    int i, j;

    for (i = 0; i < DIM; i++)
        for (j = 0; j < DIM; j++)
        {
            campo[i][j].x = i + 1;
            campo[i][j].y = j + 1;
            campo[i][j].stato = 0;
        }
}

void initFlotta(nave flotta[FSIZE])
{
    int i, j, ni, nj;

    //Impostazione dei valori standard "comuni" a tutte le navi (flag, dimensione, stato delle relative caselle)
    for (i = 0; i < FSIZE; i++)
    {
        flotta[i].flag = 0;
        //Ovviamente la dimensione varia da nave a nave...
        if (i < 1)
            flotta[i].dim = 7;
        else if (i < 3)
            flotta[i].dim = 4;
        else if (i < 5)
            flotta[i].dim = 3;
        else if (i < 8)
            flotta[i].dim = 2;
        else if (i < FSIZE)
            flotta[i].dim = 1;
        //Lo stato delle singole componenti dello scafo nave per nave
        for (j = 0; j < flotta[i].dim; j++)
            flotta[i].scafo[j].stato = 0;
    }

    /*Definizione delle caselle occupate, a partire da quella più in alto, da sinistra verso destra
    Le navi si suppongono attaccate al margine superiore sinistro del campo*/

    //Portaerei
    ni = 0;
    nj = 0;
    flotta[ni].scafo[nj].x = 0;
    flotta[ni].scafo[nj].y = 1;
    nj++;
    flotta[ni].scafo[nj].x = 0;
    flotta[ni].scafo[nj].y = 3;
    for (i = 0; i < 5; i++)
    {
        nj++;
        flotta[ni].scafo[nj].x = 1;
        flotta[ni].scafo[nj].y = i;
    }

    //Incrociatori
    while (ni < 3)
    {
        ni++;
        nj = 0;
        flotta[ni].scafo[nj].x = 0;
        flotta[ni].scafo[nj].y = 1;
        for (i = 0; i < 3; i++)
        {
            nj++;
            flotta[ni].scafo[nj].x = 1;
            flotta[ni].scafo[nj].y = i;
        }
    }

    //Cacciatorpediniere
    while (ni < 5)
    {
        nj = 0;
        for (i = 0; i < 3; i++)
        {
            flotta[ni].scafo[nj].x = 0;
            flotta[ni].scafo[nj].y = i;
            nj++;
        }
        ni++;
    }

    //Torpediniere
    while (ni < 8)
    {
        nj = 0;
        for (i = 0; i < 2; i++)
        {
            flotta[ni].scafo[nj].x = 0;
            flotta[ni].scafo[nj].y = i;
            nj++;
        }
        ni++;
    }

    //Scialuppe
    while (ni < 12)
    {
        nj = 0;
        flotta[ni].scafo[nj].x = 0;
        flotta[ni].scafo[nj].y = 0;
        ni++;
    }
}

int setFlotta(casella campo[DIM][DIM], nave flotta[FSIZE], int rand)
{

    int i, flag, adim = NCAS, stop = 0; //Numero delle caselle libere
    casella allow[NCAS], cp[NCAS];      //Lista delle caselle libere e delle caselle bloccate
    casella offset;                     //Un offset che indica la posizione della nave nel campo, in alto a sinistra

    //Azzeramento caselle bloccate
    for (i = 0; i < NCAS; i++)
    {
        allow[i].x = i / DIM;
        allow[i].y = i % DIM;
        cp[i].x = -1;
        cp[i].y = -1;
    }
    //Lo stato della prima casella viene usato come indice
    cp[0].stato = 0;

    //Posizionamento delle navi nel campo
    for (i = 0; i < FSIZE; i++)
    {
        //Modalità casuale
        if (rand)
            do
            {
                //Genera una casella casuale, includendo solo quelle non occupate
                randCell(&offset, allow, adim);
                if (offset.stato)
                    ruotaNave(&flotta[i]);
                //A volte anche se ci sono caselle libere, non sono contigue, quindi  la generazione proseguirebbe all'infinito
                stop++;
                if (stop > adim << 3)
                    return 0;
            } while (!freeCell(offset, cp, flotta[i].dim)); //Controlla che la nave non finisca in mezzo ad un'altra
        //Modalità manuale
        else
            do
            {
                printf("Posiziona %s:\n", nomeNave(i));
                drawCampo(campo, 1);
                offset = getCasella(1);
                if (offset.stato)
                    ruotaNave(&flotta[i]);
                flag = freeCell(offset, cp, flotta[i].dim);
                system(CLEAR);
                if (!flag)
                {
                    printf("CASELLA NON VALIDA!\n");
                    if (offset.stato)
                        ruotaNave(&flotta[i]);
                }
            } while (!flag);
        //Posiziona la nave nel campo ed aggiorna la lista delle caselle occupate e delle caselle libere
        placeNave(campo, &flotta[i], offset, allow, &adim, cp);
        //Il campo può essere pieno prima del posizionamento di tutte le navi
        if (i < FSIZE - 1 && adim < flotta[i + 1].dim)
            return 0;
    }
    return 1;
}

void randCell(casella *out, casella *allow, int adim)
{
    int temp;

    temp = rand() % adim;
    out->x = allow[temp].x;
    out->y = allow[temp].y;
    out->stato = rand() % 2;
}

void placeNave(casella campo[DIM][DIM], nave *ship, casella offset, casella *allow, int *adim, casella cp[NCAS])
{
    int i;

    for (i = 0; i < ship->dim; i++)
    {
        //Sposta tutti i pezzi dello scafo in base all'offset
        ship->scafo[i].x += offset.x;
        ship->scafo[i].y += offset.y;
        campo[ship->scafo[i].x][ship->scafo[i].y].stato = 1;
        removeCell(allow, adim, ship->scafo[i], cp);
    }
}

void removeCell(casella *vc, int *vdim, casella n, casella cp[NCAS])
{
    int i, k = cp[0].stato;

    for (i = 0; i < *vdim; i++)
        if (toccano(vc[i], n, 1))
        {
            cp[k] = vc[i];
            k++;
            vc[i] = vc[*vdim - 2];
            *vdim -= 1;
            i--;
        }
    cp[0].stato = k;
}

int freeCell(casella c, casella cp[NCAS], int dim)
{
    int i;
    casella cs; //casella ausiliaria

    if (c.stato)
    {
        cs.x = c.y;
        cs.y = c.x;
    }
    else
    {
        cs = c;
    }

    //Controllo bordi
    switch (dim)
    {
    case 1:
        break;
    case 2:
        if (cs.y + 1 >= DIM)
            return 0;
        break;
    case 3:
        if (cs.y + 2 >= DIM)
            return 0;
        break;
    case 4:
        if (cs.x + 1 >= DIM || cs.y + 2 >= DIM)
            return 0;
        break;
    case 7:
        if (cs.x + 1 >= DIM || cs.y + 4 >= DIM)
            return 0;
        break;
    }

    cs.x = c.x;
    cs.y = c.y;
    //Il ciclo prosegue fino a quando non trova una casella ancora da inizializzare (-1)
    for (i = 0; cp[i].x >= 0 && i < NCAS; i++)
        //Il controllo è diverso a seconda della nave
        switch (dim)
        {
        case 1: //Scialuppa
            if (coincidono(cs, cp[i]))
                return 0;
            break;
        case 2: //Cacciatorpediniere
            while (cs.x - c.x < 2 && cs.y - c.y < 2)
            {
                if (coincidono(cs, cp[i]))
                    return 0;
                if (c.stato)
                    cs.x++;
                else
                    cs.y++;
            }
            cs.x = c.x;
            cs.y = c.y;
            break;
        case 3: //Torpediniere
            while (cs.x - c.x < 3 && cs.y - c.y < 3)
            {
                if (coincidono(cs, cp[i]))
                    return 0;
                if (c.stato)
                    cs.x++;
                else
                    cs.y++;
            }
            cs.x = c.x;
            cs.y = c.y;
            break;
        case 4: //Incrociatore
            if (c.stato)
            {
                cs.x++;
                if (coincidono(cs, cp[i]))
                    return 0;
                cs.y++;
                cs.x--;
            }
            else
            {
                cs.y++;
                if (coincidono(cs, cp[i]))
                    return 0;
                cs.y--;
                cs.x++;
            }
            while (cs.x - c.x < 3 && cs.y - c.y < 3)
            {
                if (coincidono(cs, cp[i]))
                    return 0;
                if (c.stato)
                    cs.x++;
                else
                    cs.y++;
            }
            cs.x = c.x;
            cs.y = c.y;
            break;
        //La portaerei è la prima nave ad essere posizionata, non può andare in conflitto con altre navi
        //In caso di errore evita di piazzare la nave
        default:
            return 0;
        }
    //Nel caso in cui tutto vada bene
    return 1;
}

int toccano(casella c, casella cp, int angoli)
{
    //Se la casella coincide con quella occupata o con una adiacente ritorna 1, altrimenti ritorna 0
    if (angoli)
        return (c.x == cp.x && c.y == cp.y) || (c.x == cp.x - 1 && c.y == cp.y - 1) || (c.x == cp.x - 1 && c.y == cp.y) || (c.x == cp.x - 1 && c.y == cp.y + 1) || (c.x == cp.x && c.y == cp.y - 1) || (c.x == cp.x && c.y == cp.y + 1) || (c.x == cp.x + 1 && c.y == cp.y - 1) || (c.x == cp.x + 1 && c.y == cp.y) || (c.x == cp.x + 1 && c.y == cp.y + 1);
    else
        return (c.x == cp.x && c.y == cp.y) || (c.x == cp.x - 1 && c.y == cp.y) || (c.x == cp.x && c.y == cp.y - 1) || (c.x == cp.x && c.y == cp.y + 1) || (c.x == cp.x + 1 && c.y == cp.y);
}

int coincidono(casella c, casella cp)
{
    return c.x == cp.x && c.y == cp.y;
}

void drawCampo(casella campo[DIM][DIM], int show)
{
    int i, j;
    char c = 'A';

    printf("Campo ");
    if (show)
        printf("Giocatore:\n\n");
    else
        printf("Computer:\n\n");
    printf("  ");
    //Stampo i numeri
    for (i = 1; i <= DIM; i++)
        printf(" %d ", i);
    for (i = 0; i < DIM; i++)
    {
        //Stampo le lettere
        printf("\n%c ", c);
        c++;
        //Stampo le caselle
        for (j = 0; j < DIM; j++)
            switch (campo[i][j].stato)
            {
            case 0:
                printf(" _ ");
                break;
            case 1:
                if (show)
                    printf(" + ");
                else
                    printf(" _ ");
                break;
            case 2:
                printf(" . ");
                break;
            case 3:
                printf(" * ");
                break;
            case 4:
                printf(" X ");
                break;
            default:
                printf(" ? ");
                break;
            }
    }
    printf("\n\n");
}

int editCampo(casella campo[DIM][DIM], nave flotta[FSIZE], int level)
{
    casella t;
    int colpo, i;

    //Input giocatore
    if (!level)
        //Chiede una casella non ancora colpita al giocatore
        do
        {
            t = getCasella(0);
            if (campo[t.x][t.y].stato > 1)
                printf("Casella già colpita, sceglierne un'altra...\n");
        } while (campo[t.x][t.y].stato > 1);
    //Computer
    else
        t = IAshoot(campo, level);
    //Valuta se è stata colpita una nave, e aggiorna lo stato del campo in base al ritorno
    colpo = colpisciNave(t, flotta);
    if (colpo < 2)
        campo[t.x][t.y].stato = 2 + colpo;
    else
    {
        colpo -= 2;
        for (i = 0; i < flotta[colpo].dim; i++)
            //Roba da matti, eh?
            campo[flotta[colpo].scafo[i].x][flotta[colpo].scafo[i].y].stato = 4;
    }
    if (level)
    {
        printf("Il computer ha sparato in %c%d: ", t.x + 'A', t.y + 1);
        switch (campo[t.x][t.y].stato)
        {
        case 2:
            printf("Acqua!");
            break;
        case 3:
            printf("Colpito!");
            break;
        case 4:
            printf("Colpito ed affondato!");
            break;
        default:
            printf("Stato della casella sconosciuto...");
            break;
        }
        printf("\n");
        //Lascia al giocatore il tempo di leggere
        wait(3000, 1);
    }
    //Ritorna se il gioco può continuare o no (1, 0)
    return !flottaAffondata(flotta);
}

casella getCasella(int place)
{
    char tempX, maxL = 'A' + DIM - 1, vert;
    casella target;

    if (place)
        printf("Scegli una casella libera (A-%c, 1-%d).\nInserire \' dopo la casella per posizionare la nave in verticale:\n", maxL, DIM);
    else
        printf("Scegli casella da attaccare (A-%c, 1-%d):\n", maxL, DIM);
    //Accetta sia maiuscole che minuscole
    do
    {
        fflush(stdout);
        scanf("%c%d%c", &tempX, &target.y, &vert);
        if ((tempX < 'A' || (tempX > maxL && tempX < 'a') || tempX > DIM - 1 + 'a') || (target.y <= 0 || target.y > DIM))
        {
            printf("Casella non valida, riprova: (lettere da \'A\' ");
            if (maxL == 'E' || maxL == 'I' || maxL == 'O' || maxL == 'U')
                printf("ad ");
            else
                printf("a ");
            printf("\'%c\', numeri da \'1\' a \'%d\')\n", maxL, DIM);
        }
    } while ((tempX < 'A' || (tempX > maxL && tempX < 'a') || tempX > DIM - 1 + 'a') || (target.y <= 0 || target.y > DIM));

    //Distinguo maiuscole da minusacole, e normalizzo rispetto a 0
    if (tempX > maxL)
        target.x = (int)tempX - 'a';
    else
        target.x = (int)tempX - 'A';
    if (vert == '\'')
        target.stato = 1;
    else
        target.stato = 0;
    target.y--;

    return target;
}

int colpisciNave(casella t, nave flotta[FSIZE])
{
    int i, j, cea = 0;

    //Cerca fra tutte le navi
    for (i = 0; i < FSIZE; i++)
        for (j = 0; j < flotta[i].dim; j++)
            //Se la casella è occupata da una nave, cambia il suo stato
            if (coincidono(t, flotta[i].scafo[j]))
            {
                flotta[i].scafo[j].stato = 1;
                //Controlla se la nave è stata affondata
                if (affondaNave(&flotta[i]))
                    cea += 2 + i;
                else
                    cea++;
            }

    return cea;
}

int affondaNave(nave *n)
{
    int i;

    for (i = 0; i < n->dim; i++)
        if (!n->scafo[i].stato)
            return 0;
    if (i == n->dim)
        n->flag = 1;
    return 1;
}

int flottaAffondata(nave flotta[FSIZE])
{
    int i;
    for (i = 0; i < FSIZE; i++)
        if (!flotta[i].flag)
            return 0;
    return 1;
}

void tutorial(void)
{
    FILE *fT;
    char c, tut;

    printf("Vuoi visualizzare un breve tutorial sul posizionamento delle navi? (y/n)\n");
    do
    {
        scanf("%c", &tut);
        flusher();
        if (tut != 'y' && tut != 'n')
            printf("Opzione non valida. Vuoi visualizzare il tutorial? (\'y\' = sì, \'n\' = no)\n");
    } while (tut != 'y' && tut != 'n');
    if (tut == 'y')
    {
        system(CLEAR);
        fT = openFile("tutorial.txt", "rt");
        copiaFile(fT, stdout);
        printf("Premere un tasto per chiudere il tutorial:\n");
        scanf("%c", &c);
    }
    system(CLEAR);
}

char *nomeNave(int n)
{
    switch (n)
    {
    case 0:
        return "la portaerei";
    case 1:
        return "il primo incrociatore";
    case 2:
        return "il secondo incrociatore";
    case 3:
        return "il primo torpediniere";
    case 4:
        return "il secondo torpediniere";
    case 5:
        return "il primo cacciatorpediniere";
    case 6:
        return "il secondo cacciatorpediniere";
    case 7:
        return "l\'ultimo cacciatorpediniere";
    case 8:
        return "la prima scialuppa";
    case 9:
        return "la seconda scialuppa";
    case 10:
        return "la terza scialuppa";
    case 11:
        return "l\'ultima scialuppa";
    default:
        return "nave";
    }
}

casella IAshoot(casella campo[DIM][DIM], int level)
{
    int i, j, k = 0, flag = 1, adim = 0, cdim = 0, affondate = 0, acqua = 0; //Numero delle caselle libere
    casella allow[NCAS], colpite[SIZE];                                      //Lista delle caselle libere e di quelle già colpite
    casella target;

    //Azzeramento stato caselle da colpire
    for (i = 0; i < NCAS; i++)
        colpite[i].stato = 0;
    //Caricamento caselle già colpite e libere
    for (i = 0; i < DIM; i++)
        for (j = 0; j < DIM; j++)
        {
            if (campo[i][j].stato == 3)
            {
                cdim++;
                colpite[k - adim - affondate - acqua].x = i;
                colpite[k - adim - affondate - acqua].y = j;
            }
            //Escludo le caselle colpite a vuoto
            else if (campo[i][j].stato == 2)
                acqua++;
            //Escludo le navi affondate e le caselle circostanti
            else if (campo[i][j].stato == 4)
            {
                if (!i && !j && (campo[i][j + 1].stato == 4 || campo[i + 1][j].stato == 4 || campo[i + 1][j + 1].stato == 4))
                    affondate++;
                else if (i == DIM - 1 && !j && (campo[i - 1][j].stato == 4 || campo[i - 1][j + 1].stato == 4 || campo[i][j + 1].stato == 4))
                    affondate++;
                else if (!i && j == DIM - 1 && (campo[i][j - 1].stato == 4 || campo[i + 1][j - 1].stato == 4 || campo[i + 1][j].stato == 4))
                    affondate++;
                else if (i == DIM - 1 && j == DIM - 1 && (campo[i - 1][j - 1].stato == 4 || campo[i - 1][j].stato == 4 || campo[i][j - 1].stato == 4))
                    affondate++;
                else if (!i && (campo[i][j - 1].stato == 4 || campo[i][j + 1].stato == 4 || campo[i + 1][j - 1].stato == 4 || campo[i + 1][j].stato == 4 || campo[i + 1][j + 1].stato == 4))
                    affondate++;
                else if (!j && (campo[i - 1][j].stato == 4 || campo[i - 1][j + 1].stato == 4 || campo[i][j + 1].stato == 4 || campo[i + 1][j].stato == 4 || campo[i + 1][j + 1].stato == 4))
                    affondate++;
                else if (i == DIM - 1 && (campo[i - 1][j - 1].stato == 4 || campo[i - 1][j].stato == 4 || campo[i - 1][j + 1].stato == 4 || campo[i][j - 1].stato == 4 || campo[i][j + 1].stato == 4))
                    affondate++;
                else if (j == DIM - 1 && (campo[i - 1][j - 1].stato == 4 || campo[i - 1][j].stato == 4 || campo[i][j - 1].stato == 4 || campo[i + 1][j - 1].stato == 4 || campo[i + 1][j].stato == 4))
                    affondate++;
                else if (i && j && i < DIM - 1 && j < DIM - 1 && (campo[i - 1][j - 1].stato == 4 || campo[i - 1][j].stato == 4 || campo[i - 1][j + 1].stato == 4 || campo[i][j - 1].stato == 4 || campo[i][j + 1].stato == 4 || campo[i + 1][j - 1].stato == 4 || campo[i + 1][j].stato == 4 || campo[i + 1][j + 1].stato == 4))
                    affondate++;
            }
            else
            {
                adim++;
                allow[k - cdim - affondate - acqua].x = i;
                allow[k - cdim - affondate - acqua].y = j;
                allow[k - cdim - affondate - acqua].stato = 0;
            }
            k++;
        }
    //Switch sulla difficoltà
    do
    {
        switch (level)
        {
        case 1:
            randCell(&target, allow, adim);
            break;
        case 2:
            if (!cdim)
            {
                randCell(&target, allow, adim);
                flag = 0;
            }
            else
                for (i = 0; i < adim && flag; i++)
                    for (j = 0; j < cdim; j++)
                        if (toccano(allow[i], colpite[j], 0))
                        {
                            target.x = allow[i].x;
                            target.y = allow[i].y;
                            flag = 0;
                            break;
                        }
            break;
        case 3:
            if (!cdim)
                randCell(&target, allow, adim);
            else
                switch (cdim)
                {
                case 1:
                    if (campo[colpite[0].x][colpite[0].y - 1].stato > 1 && campo[colpite[0].x][colpite[0].y + 1].stato > 1)
                    {
                        target.x = colpite[0].x + 1;
                        target.y = colpite[0].y;
                    }
                    else
                        for (i = 0; i < adim; i++)
                            if (allow[i].x == colpite[0].x && (allow[i].y == colpite[0].y - 1 || allow[i].y == colpite[0].y + 1))
                            {
                                target.x = allow[i].x;
                                target.y = allow[i].y;
                                break;
                            }
                    break;
                case 2:
                    for (i = 0; i < adim; i++)
                        if ((colpite[0].x == colpite[1].x && colpite[0].x == allow[i].x) && (allow[i].y == colpite[0].y - 1 || allow[i].y == colpite[1].y + 1))
                        {
                            target.x = allow[i].x;
                            target.y = allow[i].y;
                            break;
                        }
                        else if (allow[i].x == colpite[1].x && (allow[i].y == colpite[1].y - 1 || allow[i].y == colpite[1].y + 1))
                        {
                            target.x = allow[i].x;
                            target.y = allow[i].y;
                            break;
                        }
                    break;
                case 3:
                    if (colpite[0].x == colpite[1].x && colpite[0].x == colpite[2].x && campo[colpite[1].x - 1][colpite[1].y].stato > 1)
                    {
                        target.x = colpite[0].x;
                        target.y = colpite[0].y - 1;
                    }
                    else
                        for (i = 0; i < adim; i++)
                            if (colpite[0].x == colpite[1].x && colpite[0].x == colpite[2].x && allow[i].y == colpite[1].y && allow[i].x == colpite[1].x - 1)
                            {
                                target.x = allow[i].x;
                                target.y = allow[i].y;
                                break;
                            }
                            else if (colpite[0].x != colpite[1].x && colpite[2].x == allow[i].x && allow[i].y == colpite[2].y + 1)
                            {
                                target.x = allow[i].x;
                                target.y = allow[i].y;
                                break;
                            }
                    break;
                case 4:
                    if (colpite[0].x == colpite[1].x && colpite[0].x == colpite[2].x && colpite[0].x == colpite[3].x)
                    {
                        target.x = colpite[3].x;
                        target.y = colpite[3].y + 1;
                    }
                    else
                        for (i = 0; i < adim; i++)
                            if (allow[i].x == colpite[1].x && (allow[i].y == colpite[1].y - 1 || allow[i].y == colpite[3].y + 1))
                            {
                                target.x = allow[i].x;
                                target.y = allow[i].y;
                                break;
                            }
                    break;
                case 5:
                    if (colpite[0].x == colpite[1].x && colpite[0].x == colpite[2].x && colpite[0].x == colpite[3].x && colpite[0].x == colpite[4].x)
                    {
                        target.x = colpite[1].x - 1;
                        target.y = colpite[1].y;
                    }
                    else
                        for (i = 0; i < adim; i++)
                            if ((allow[i].x == colpite[1].x - 1 && allow[i].y == colpite[1].y) || (allow[i].x == colpite[4].x - 1 && allow[i].y == colpite[4].y))
                            {
                                target.x = allow[i].x;
                                target.y = allow[i].y;
                                break;
                            }
                    break;
                case 6:
                    if (colpite[1].x == colpite[2].x && colpite[1].x == colpite[3].x && colpite[1].x == colpite[4].x && colpite[1].x == colpite[5].x)
                    {
                        target.x = colpite[4].x - 1;
                        target.y = colpite[4].y;
                    }
                    else
                        for (i = 0; i < adim; i++)
                            if (allow[i].x == colpite[2].x && (allow[i].y == colpite[2].y - 1 || allow[i].y == colpite[5].y + 1))
                            {
                                target.x = allow[i].x;
                                target.y = allow[i].y;
                                break;
                            }
                    break;
                default:
                    level = 2;
                    break;
                }
            break;
        default:
            randCell(&target, allow, adim);
            break;
        }
    } while (flag && level == 2);
    return target;
}

void ruotaNave(nave *ship)
{
    int i, temp;

    for (i = 0; i < ship->dim; i++)
    {
        temp = ship->scafo[i].x;
        ship->scafo[i].x = ship->scafo[i].y;
        ship->scafo[i].y = temp;
    }
}