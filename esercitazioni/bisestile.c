//scansiona un anno e stampa se esso è bisestile oppure no
#include <stdio.h>

int main(void)
{

    int anno, ds1 = 4, ds2 = 100, ds3 = 400, val1, val2, val3;

    printf("Inserire anno: ");
    scanf("%d", &anno);

    val1 = anno % ds1;
    val2 = anno % ds2;
    val3 = anno % ds3;
    if (val1==0 && val2 != 0 || val3==0)
        printf("L'anno è bisestile!\n");
    else
        printf("L'anno non è bisestile\n");
}