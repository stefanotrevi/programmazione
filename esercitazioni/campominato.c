#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NRIGHE 20
#define NCOLONNE 20

typedef struct
{
    int mina;
    int mineAdiacenti;
    int mark;
    int discovered;
    int bU;
    int bD;
    int bL;
    int bR;
} campo;
typedef struct
{
    int x;
    int y;
    char what;
} punto;

void intro(void);
void victory(void);
void defeat(void);
void initialize(int *dh, int *dw, int *lvl);
void inizializzaCampo(campo c[][NCOLONNE], int dh, int dw);
void genMine(campo c[][NCOLONNE], int dh, int dw, int difficulty, int *nMine);
void findMine(campo c[NRIGHE][NCOLONNE], int dh, int dw);
void genCampo(campo c[NRIGHE][NCOLONNE], int dh, int dw, int difficulty, int *nMine);
void chooseASide(campo c[][NCOLONNE], char pc[][NCOLONNE], int dh, int dw, punto *P);
void genPseudoCampo(char pc[NRIGHE][NCOLONNE], int dh, int dw);
void printCharMatrix(char pc[][NCOLONNE], int dh, int dw);
void getPunto(punto *P, campo c[][NCOLONNE], int dh, int dw);
void editPunto(campo c[][NCOLONNE], char pc[][NCOLONNE], int *i, int *j, punto *P);
int checkMine(campo c[NRIGHE][NCOLONNE], punto P, int dh, int dw, int *nMine);
void scopriCasella(campo c[][NCOLONNE], char pc[][NCOLONNE], punto P);
void scopriAdiacenti(campo c[][NCOLONNE], char pc[][NCOLONNE], punto P);
void editPseudoCampo(char pc[][NCOLONNE], campo c[][NCOLONNE], punto P, int dh, int dw, int validator);
void markMine(campo c[][NCOLONNE], punto *P, char pc[][NCOLONNE], int dh, int dw);
void unmarkMine(campo c[][NCOLONNE], punto *P, char pc[][NCOLONNE], int dh, int dw);

int main(void)
{
    int i, j, dh, dw, lvl, nMine, lost;
    punto P;
    campo campo[NRIGHE][NCOLONNE];
    char pseudocampo[NRIGHE][NCOLONNE], quit;

start:
    nMine = 0;
    intro();
    initialize(&dh, &dw, &lvl);
    do
    {
        genCampo(campo, dh, dw, lvl, &nMine);
    } while (!nMine);
    genPseudoCampo(pseudocampo, dh, dw);

    do
    {
        chooseASide(campo, pseudocampo, dh, dw, &P);
        lost = checkMine(campo, P, dh, dw, &nMine);
        editPseudoCampo(pseudocampo, campo, P, dh, dw, lost);
        lost = checkMine(campo, P, dh, dw, &nMine);
    } while (!lost);

    if (lost == -1)
        victory();
    else
        defeat();
    printf("premi 'q' per terminare, 'r' per ricominciare...\n");
    scanf(" %c", &quit);
    if (quit == 'r')
        goto start;

    return 0;
}

void intro(void)
{
    char start;
    //solo per windows CMD
    system("chcp 65001");
    //solo per Windows Powershell
    system("[Console]::OutputEncoding = [Text.UTF8Encoding]::UTF8");
    system("cls");

    printf("Benvenuti a campo minato! Pronti a iniziare? (y/n)\n");
    scanf(" %c", &start);
    while (start != 'y' && start != 'n')
    {
        printf("Per iniziare, premere 'y', per uscire, premere 'n'\n");
        scanf(" %c", &start);
        if (start == 'n')
            exit(-1);
        if (start == 'y')
            break;
    }
}

void victory(void)
{
    printf("Congratulazioni, hai vinto la partita!!!\n");
}
void defeat(void)
{
    printf("Che peccato, hai perso! Andrà meglio la prossima volta :)\n");
}
void initialize(int *dh, int *dw, int *lvl)
{
    do
    {
        printf("Inserire altezza campo (massimo 20):\n");
        scanf("%d", dh);
        printf("Inserire larghezza campo (massimo 20):\n");
        scanf("%d", dw);
        if (*dh > 20 || *dw > 20 || *dh <= 0 || *dw <= 0)
            printf("I dati inseriti non sono validi, riprova:\n");
    } while (*dh > 20 || *dw > 20 || *dh <= 0 || *dw <= 0);
    do
    {
        printf("Scegliere difficoltà (1-10):\n");
        scanf("%d", lvl);
        if (*lvl < 1 || *lvl > 10)
            printf("La difficoltà impostata non è valida, riprova:\n");
    } while (*lvl < 1 || *lvl > 10);
    printf("Generazione campo in corso!\n");
}

void inizializzaCampo(campo c[][NCOLONNE], int dh, int dw)
{
    int i, j;
    for (i = 0; i < dh; i++)
        for (j = 0; j < dw; j++)
        {
            c[i][j].mark = 0;
            c[i][j].mineAdiacenti = 0;
            c[i][j].discovered = 0;
            c[i][j].bU = 0;
            c[i][j].bD = 0;
            c[i][j].bL = 0;
            c[i][j].bR = 0;
            if (i == 0)
                c[i][j].bU = 1;
            if (j == 0)
                c[i][j].bL = 1;
            if (i == dh - 1)
                c[i][j].bD = 1;
            if (j == dw - 1)
                c[i][j].bR = 1;
        }
}

void genMine(campo c[][NCOLONNE], int dh, int dw, int difficulty, int *nMine)
{
    int mina, i, j;
    srand(time(NULL));
    for (i = 0; i < dh; i++)
        for (j = 0; j < dw; j++)
            if (0 + rand() % (100 + 1) < difficulty * 3)
            {
                c[i][j].mina = 1;
                *nMine = *nMine + 1;
            }
            else
                c[i][j].mina = 0;
}

void findMine(campo c[NRIGHE][NCOLONNE], int dh, int dw)
{
    int i, j;
    for (i = 0; i < dh; i++)
        for (j = 0; j < dw; j++)
        {
            if (!c[i][j].mina)
            {
                //quadrato interno
                if (!c[i][j].bU && !c[i][j].bD && !c[i][j].bL && !c[i][j].bR)
                {
                    if (c[i - 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //alto sinistra
                else if (c[i][j].bU && c[i][j].bL)
                {
                    if (c[i][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //basso destra
                else if (c[i][j].bD && c[i][j].bR)
                {
                    if (c[i - 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //alto destra
                else if (c[i][j].bU && c[i][j].bR)
                {
                    if (c[i][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //basso sinistra
                else if (c[i][j].bD && c[i][j].bL)
                {
                    if (c[i - 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //cornice su
                else if (c[i][j].bU)
                {
                    if (c[i][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //cornice sinistra
                else if (c[i][j].bL)
                {
                    if (c[i - 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    else if (i < dh && j == dw)
                    {
                        if (c[i - 1][j - 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i - 1][j].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i][j - 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i + 1][j - 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i + 1][j].mina == 1)
                            c[i][j].mineAdiacenti++;
                    }
                    else if (i == dh && j < dw)
                    {
                        if (c[i - 1][j - 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i - 1][j].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i - 1][j + 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i][j + 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                        if (c[i][j - 1].mina == 1)
                            c[i][j].mineAdiacenti++;
                    }
                }
                //cornice giù
                else if (c[i][j].bD)
                {
                    if (c[i - 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j + 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
                //cornice destra
                else if (c[i][j].bR)
                {
                    if (c[i - 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i - 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j - 1].mina == 1)
                        c[i][j].mineAdiacenti++;
                    if (c[i + 1][j].mina == 1)
                        c[i][j].mineAdiacenti++;
                }
            }
        }
}

void genCampo(campo c[][NCOLONNE], int dh, int dw, int difficulty, int *nMine)
{
    inizializzaCampo(c, dh, dw);
    genMine(c, dh, dw, difficulty, nMine);
    findMine(c, dh, dw);
}
void genPseudoCampo(char pc[NRIGHE][NCOLONNE], int dh, int dw)
{
    int i, j;
    for (i = 0; i < dh; i++)
        for (j = 0; j < dw; j++)
            pc[i][j] = '-';
    printCharMatrix(pc, dh, dw);
}
void printCharMatrix(char pc[][NCOLONNE], int dh, int dw)
{
    int i, j;
    for (i = 0; i < dw; i++)
        printf("__");
    printf("\n");
    for (i = 0; i < dh; i++)
    {
        for (j = 0; j < dw; j++)
            if (pc[i][j] == '*' || pc[i][j] == '-' || pc[i][j] == '?')
                printf("%c ", pc[i][j]);
            else if (pc[i][j] == 0)
                printf("0 ");
            else
                printf("%d ", pc[i][j]);
        printf("\n");
    }
}

void chooseASide(campo c[][NCOLONNE], char pc[][NCOLONNE], int dh, int dw, punto *P)
{
    printf("Vuoi marcare, smarcare o aprire una casella? (m / s / a)\n");
    do
    {
        scanf(" %c", &P->what);
        fflush(stdin);
        if (P->what == 'a')
            getPunto(P, c, dh, dw);
        else if (P->what == 'm')
            markMine(c, P, pc, dh, dw);
        else if (P->what == 's')
            unmarkMine(c, P, pc, dh, dw);
        else
            printf("Per aprire una casella, premere 'a', per marcare una mina, premere 'm', per smarcare una casella, premere 's':\n");
    } while (P->what != 'm' && P->what != 'a' && P->what != 's');
}
void getPunto(punto *P, campo c[][NCOLONNE], int dh, int dw)
{
    printf("Inserire le coordinate di una casella (da 1 a %d per l'altezza, da 1 a %d per la larghezza):\n", dh, dw);
    do
    {
        scanf("%d%d", &P->x, &P->y);
        P->x--;
        P->y--;
        if (P->x > dh || P->x < 0 || P->y < 0 || P->y > dw)
            printf("Almeno una coordinata non è valida, riprova:\n");
        if (c[P->x][P->y].mark)
            printf("La casella inserita è stata marcata, selezionarne una diversa:\n");
    } while (P->x > dh || P->x < 0 || P->y < 0 || P->y > dw || c[P->x][P->y].mark);
}

int checkMine(campo c[NRIGHE][NCOLONNE], punto P, int dh, int dw, int *nMine)
{
    int i, j, lost = 0, win, nMT = 0;

    if (P.what == 'a')
    {
        if (c[P.x][P.y].mina == 1)
            lost = 1;
        else
            lost = 0;
    }
    else if (P.what == 'm')
    {
        for (i = 0; i < dh; i++)
            for (j = 0; j < dw; j++)
                if (c[i][j].mina == c[i][j].mark && c[i][j].mark == 1)
                    nMT++;
        if (nMT == *nMine)
            lost = -1;
    }
    return lost;
}

void editPseudoCampo(char pc[][NCOLONNE], campo c[][NCOLONNE], punto P, int dh, int dw, int validator)
{
    int i, j, k = 0;
    punto R;

    if (P.what == 'a')
    {
        if (validator)
            for (i = 0; i < dh; i++)
                for (j = 0; j < dw; j++)
                    if (c[i][j].mina)
                        pc[i][j] = '*';
                    else
                        pc[i][j] = '-';
        else
        {
            scopriCasella(c, pc, P);
            for (i = 0; i < dh; i++)
                for (j = 0; j < dw; j++)
                    if (pc[i][j] == 0 && c[i][j].discovered == 0)
                    {
                        c[i][j].discovered = 1;
                        P.x = i;
                        P.y = j;
                        scopriAdiacenti(c, pc, P);
                        i = 0;
                        j = 0;
                    }
        }
    }
    printCharMatrix(pc, dh, dw);
}

void scopriCasella(campo c[][NCOLONNE], char pc[][NCOLONNE], punto P)
{
    if (!c[P.x][P.y].mina)
        pc[P.x][P.y] = c[P.x][P.y].mineAdiacenti;
}
void scopriAdiacenti(campo c[][NCOLONNE], char pc[][NCOLONNE], punto P)
{
    int i, j, bU = c[P.x][P.y].bU, bD = c[P.x][P.y].bD, bL = c[P.x][P.y].bL, bR = c[P.x][P.y].bR;

    if (!bU && !bL && !bR && !bD)
        for (i = -1; i <= 1; i++)
            for (j = -1; j <= 1; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bU && bL)
        for (i = 0; i <= 1; i++)
            for (j = 0; j <= 1; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bU && bR)
        for (i = 0; i <= 1; i++)
            for (j = -1; j <= 0; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bD && bL)
        for (i = -1; i <= 0; i++)
            for (j = 0; j <= 1; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bD && bR)
        for (i = -1; i <= 0; i++)
            for (j = -1; j <= 0; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bU && !bL && !bR)
        for (i = 0; i <= 1; i++)
            for (j = -1; j <= 1; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bL && !bU && !bD)
        for (i = -1; i <= 1; i++)
            for (j = 0; j <= 1; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bR && !bU && !bD)
        for (i = -1; i <= 1; i++)
            for (j = -1; j <= 0; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);

    if (bD && !bL && !bR)
        for (i = -1; i <= 0; i++)
            for (j = -1; j <= 1; j++)
                if (!c[P.x + i][P.y + j].mina && (i || j))
                    editPunto(c, pc, &i, &j, &P);
}

void editPunto(campo c[][NCOLONNE], char pc[][NCOLONNE], int *i, int *j, punto *P)
{
    punto Q;
    Q.x = P->x + *i;
    Q.y = P->y + *j;
    scopriCasella(c, pc, Q);
}

void markMine(campo c[][NCOLONNE], punto *P, char pc[][NCOLONNE], int dh, int dw)
{
    printf("Seleziona le coordinate della casella da marcare come mina:\n");
    do
    {
        scanf("%d%d", &P->x, &P->y);
        if (P->x > dh || P->y > dw)
            printf("Almeno una delle coordinate non è valida, riprova:\n");
    } while (P->x > dh || P->y > dw);
    P->x--;
    P->y--;
    c[P->x][P->y].mark = 1;
    c[P->x][P->y].mark = 1;
    pc[P->x][P->y] = '?';
}
void unmarkMine(campo c[][NCOLONNE], punto *P, char pc[][NCOLONNE], int dh, int dw)
{
    printf("Seleziona le coordinate della casella da smarcare:\n");
    do
    {
        scanf("%d%d", &P->x, &P->y);
        if (P->x > dh || P->y > dw)
            printf("Almeno una delle coordinate non è valida, riprova:\n");
    } while (P->x > dh || P->y > dw);
    P->x--;
    P->y--;
    c[P->x][P->y].mark = 0;
    c[P->x][P->y].mark = 0;
    pc[P->x][P->y] = '-';
}
