//programma che legge una lista di due stringhe e un numero e stampa la riga in cui compare il nome immesso dall'utente
#include <stdio.h>
#include <stdlib.h>
#define DIM 40
int main(void)
{
    int i, j, num, trovato;
    char s[DIM], nome[DIM], ind[DIM];
    FILE *f;
    f = fopen("rubrica.txt", "r");
    if (f == NULL)
    {
        printf("File non trovato\n");
        exit(-1);
    }
    scanf("%s", s);

    for (i = 0; i < DIM; i++)
    {
        trovato = 1;
        fscanf(f, "%s", nome);
        fscanf(f, "%s", ind);
        fscanf(f, "%d\n", &num);
        for (j = 0; j < DIM && s[j] != '\0'; j++)
            if (s[j] != nome[j])
                trovato = 0;
        if (trovato)
        {
            printf("%s %s %d\n", nome, ind, num);
            break;
        }
    }

    fclose(f);
    return 0;
}