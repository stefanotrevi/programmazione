#include <stdio.h>
#include <stdlib.h>

int eleva(int n, int m);
int main(void)
{
    int n, m;

    printf("Inserisci base ed esponente:\n");
    scanf("%d%d", &n, &m);
    printf("%d", eleva(n, m));
}

int eleva(int n, int m)
{
    int potenza;
    if (n == 0 && m == 0)
    {
        printf("NaN\n");
        exit(1);
    }
    else if (n == 0)
        potenza = 0;
    else if (m == 1)
        potenza = n;
    else if (m == 0)
        potenza = 1;
    else
        potenza = n * eleva(n, m - 1);
    return potenza;
}