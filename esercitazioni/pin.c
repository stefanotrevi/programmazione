//scansiona un numero, fino ad un massimo di 3 volte, e verifica che sia uguale al PIN, garantendo/negando/bloccando l'accesso.
#include <stdio.h>

int main(void)
{
    int pin;
    int i = 1;

    do
    {
        printf("Inserire PIN: ");
        scanf("%d", &pin);
        if (pin == 44122)
        {
            printf("Accesso consentito\n");
            i = 5;
        }
        else
        {
            i++;
            if (i <= 3)
                printf("Riprova. Numero tentativi rimasti: %d\n", 4 - i);
        }
    } while (i <= 3);
    {
    }
    if (i == 4)
        printf("Accesso bloccato! Numero massimo di tentativi superato\n");
}