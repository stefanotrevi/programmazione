#include <stdio.h>
#define DIM 201

int bisestile(int a);

int main(void)
{
    int a[DIM], b[DIM], i, j = 0, anno = 1900;

    for (i = 0; i < DIM; i++)
    {
        a[i] = anno;
        anno++;
        /* if (bisestile(a[i]))
        {
            b[j] = a[i];
            printf("%d ", b[j]);
            j++;
        }*/
        printf("%d ", a[i]);
    }
    printf("\n");
    return 0;
}

int bisestile(int a)
{
    int ds1 = 4, ds2 = 100, ds3 = 400, val1, val2, val3, val;
    val1 = a % ds1;
    val2 = a % ds2;
    val3 = a % ds3;
    val = val1 == 0 && val2 != 0 || val3 == 0;
    return val;
}