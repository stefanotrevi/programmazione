#include <stdio.h>
#include <stdlib.h>
#include "listaCaratteri.h"

int main(int argc, char **argv)
{
    FILE *fT;
    Lista L;
    int n;

    eccezioneArg(argc, 2);
    fT = fopen(argv[1], "rt");
    eccezioneFile(fT);
    nuovaLista(&L);
    FileInsTesta(&L, fT);
    stampa(L);
    printf("Inserire numero caratteri da stampare:\n");
    scanf("%d", &n);
    stampaUltimi(n, L);
    fclose(fT);
    return 0;
}