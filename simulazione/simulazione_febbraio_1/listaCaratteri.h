//la definizione di dato può essere ampliata come struct, ma alcune
//delle procedure/funzioni hanno bisogno di essere leggermente modificate
typedef char Dato;

#define MAXN 1000

typedef struct nodo
{
    Dato dato;
    struct nodo *next;
} Nodo;

typedef Nodo *Lista;

//inizializza una lista collegata
void nuovaLista(Lista *l);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int argc, int argHp);

//controlla che i file esistano
void eccezioneFile(FILE *fT);

//inserisce i dati raccolti dal file "fT" nella lista "l", questa funzione deve essere
//modificata in modo appropriato in base ai dati che si intende raccogliere
void FileInsTesta(Lista *l, FILE *fT);

//stampa una lista "l"
void stampa(Lista l);

//inserisce il dato "n" in testa alla lista "l"
void insTesta(Lista *l, Dato n);

//inverte l'ordine degli elementi di una lista "l", inserendoli in un'altra lista
// es. se la lista è [1, 2, 3] l'output è [3, 2, 1]
Lista reverse(Lista l);

//stampa gli ultimi elementi di una lista
void stampaUltimi(int n, Lista l);