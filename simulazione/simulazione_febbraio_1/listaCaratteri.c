#include <stdio.h>
#include <stdlib.h>
#include "listaCaratteri.h"

void eccezioneArg(int argc, int argHp)
{
    if (argc != argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE *fT)
{

    if (fT == NULL)
    {
        printf("Almeno uno dei file indicati non esiste.\n");
        exit(-2);
    }
}

void nuovaLista(Lista *l)
{
    *l = NULL;
}

void FileInsTesta(Lista *l, FILE *fT)
{
    Dato d;
    while (!feof(fT))
    {

        fscanf(fT, "%c", &d);
        if (!feof(fT))
            insTesta(l, d);
    }
}

void stampa(Lista l)
{
    if (!l)
        printf("La lista è vuota.\n");
    while (l)
    {
        printf("%c", l->dato);
        l = l->next;
    }
    printf("\n");
}

void insTesta(Lista *l, Dato d)
{
    Nodo *aux = (Nodo *)malloc(sizeof(Nodo));
    aux->dato = d;
    aux->next = *l;
    *l = aux;
}

Lista reverse(Lista l)
{
    Lista Lr;
    nuovaLista(&Lr);
    while (l)
    {
        insTesta(&Lr, l->dato);
        l = l->next;
    }
    return Lr;
}

void stampaUltimi(int n, Lista l)
{
    Lista Lt;
    int i;
    nuovaLista(&Lt);
    l = reverse(l);
    while (n && l)
    {
        insTesta(&Lt, l->dato);
        l = l->next;
        n--;
    }
    stampa(Lt);
}