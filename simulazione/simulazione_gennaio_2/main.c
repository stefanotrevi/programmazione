#include <stdio.h>
#include <stdlib.h>
#include "listino.h"

int main(int argc, char **argv)
{
    FILE *fB, *fT;
    Lista Listino;
    int buff[MAXN], dimB;

    eccezioneArg(argc, 2);
    fB = fopen(argv[1], "rb");
    fT = fopen("scontrini.txt", "wt");
    eccezioneFile(fB, fT);
    nuovaLista(&Listino);
    FileInsCoda(&Listino, fB);
    dimB = nscanf(buff, MAXN, lunghezza(Listino) - 1);
    genScontrino(Listino, buff, dimB, fT);

    return 0;
}