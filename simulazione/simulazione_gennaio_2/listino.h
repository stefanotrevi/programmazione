//Questa librerie include le funzioni relative alle liste collegate

// lunghezza massima del campo "nome"
#define MAXL 30

//numero massimo di Dati in una lista (in questo caso)
#define MAXN 100

//definizione dell'ADT "cibo"
typedef struct
{
    int codice;
    char nome[MAXL];
    float prezzo;
} cibo;

//associazione del tipo "cibo" al generico "Dato"
typedef cibo Dato;

//definizione di "nodo" di una lista collegata
typedef struct nodo
{
    Dato dato;
    struct nodo *next;
} Nodo;

//definizione di lista
typedef Nodo *Lista;

//inizializza una lista collegata
void nuovaLista(Lista *l);

//inserisce il dato "n" in testa alla lista "l"
void insTesta(Lista *l, Dato n);

//inserisce "n" dati appartenenti al vettore "d" in coda alla lista "l"
void nInsCoda(Lista *l, Dato *d, int n);

//elimina la testa alla lista "l"
void eliminTesta(Lista *l);

//restituisce la lunghezza della lista "l"
int lunghezza(Lista l);

//modifica della funzione "cerca", per operare nella funzione "insCoda"
Lista *cercaCoda(Lista *l);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int argc, int argHp);

//controlla che i file esistano
void eccezioneFile(FILE *fB, FILE *fT);

//inserisce i dati raccolti dal file "fB" nella lista "l"
void FileInsCoda(Lista *l, FILE *fB);

//genera lo scontrino partendo dal pasto inserito nel vettore "pasto"
void genScontrino(Lista l, int *pasto, int dimP, FILE *fT);

//scansiona "n" elementi inserendoli nel vettore "buff", accettando valori da 0 a "cap"
int nscanf(int *buff, int n, int cap);
