#include <stdio.h>
#include <stdlib.h>
#include "listino.h"

void nuovaLista(Lista *l)
{
    *l = NULL;
}

void insTesta(Lista *l, Dato d)
{
    Nodo *aux = (Nodo *)malloc(sizeof(Nodo));
    aux->dato = d;
    aux->next = *l;
    *l = aux;
}

void nInsCoda(Lista *l, Dato *d, int n)
{
    int i;
    Lista *ls = l;
    for (i = 0; i < n; i++)
    {
        l = cercaCoda(l);
        insTesta(l, d[i]);
        l = ls;
    }
}

void eliminTesta(Lista *l)
{
    if (*l)
        *l = (*l)->next;
    else
        printf("E kome, Gokov, kome, puoi eliminave la mia tefta... Fe non ho una tefta? BUAHAHAH!\n");
}

int lunghezza(Lista l)
{
    int i = 0;
    while (l)
    {
        i++;
        l = l->next;
    }
    return i;
}

Lista *cercaCoda(Lista *l)
{
    while (*l)
        l = &(*l)->next;

    return l;
}

void eccezioneArg(int argc, int argHp)
{
    if (argc != argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE *fB, FILE *fT)
{

    if (fB == NULL || fT == NULL)
    {
        printf("Almeno uno dei file indicati non esiste.\n");
        exit(-2);
    }
}

void FileInsCoda(Lista *l, FILE *fB)
{
    Dato temp[MAXN];
    int i;

    for (i = 0; i < MAXN; i++)
        temp[i].codice = -1;
    fread(temp, sizeof(Dato), MAXN, fB);
    for (i = 0; temp[i].codice != -1; i++)
        ;
    nInsCoda(l, temp, i);
}

void genScontrino(Lista l, int *pasto, int dimP, FILE *fT)
{
    int i, n;
    float tot = 0;
    while (l)
    {
        n = 0;
        for (i = 0; i < dimP; i++)
            if (pasto[i] == l->dato.codice)
                n++;
        if (n)
        {
            tot += l->dato.prezzo * n;
            fprintf(fT, "%d x %f (%s) = %f\n", n, l->dato.prezzo, l->dato.nome, l->dato.prezzo * n);
        }
        eliminTesta(&l);
    }
    fprintf(fT, "Totale: %f", tot);
}

int nscanf(int *buff, int n, int cap)
{
    int i, temp;
    printf("Inserire fino ad %d elementi, compresi fra 0 e %d, inserire \"-1\" per terminare:\n", n, cap);
    for (i = 0; i < n && temp != -1; i++)
    {
        scanf("%d", &temp);
        if (temp >= 0 && temp <= cap)
            buff[i] = temp;
    }
    return i;
}