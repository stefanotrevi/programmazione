#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

void nuovaLista(Lista *l)
{
    *l = NULL;
}

void eccezioneArg(int argc, int argHp)
{
    if (argc != argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE *fT, FILE *fB)
{

    if (fT == NULL || fB == NULL)
    {
        printf("Almeno uno dei file indicati non esiste.\n");
        exit(-2);
    }
}

void inserisciFile(FILE *fT, Lista *l)
{
    Dato d[MAXN];
    int giorno, nMaterie = 0;
    float oraI, oraF;
    char tempS[MAXL];

    azzeraDato(d);
    while (!feof(fT))
    {
        fscanf(fT, "%s%d%f%f", tempS, &giorno, &oraI, &oraF);
        nMaterie = calcolaDato(tempS, oraI, oraF, nMaterie, d);
    }
    nInsCoda(l, d, nMaterie);
}

int confrontaStringhe(char str1[MAXL], char str2[MAXL])
{
    int i, flag = 1;
    for (i = 0; i < MAXL && str1[i] && str2[i]; i++)
        if (str1[i] != str2[i])
        {
            flag = 0;
            break;
        }
    return flag;
}

void inserisciStringa(char *str1, char *str2, int max)
{
    int i;
    for (i = 0; i < max - 1 && str1[i]; i++)
        str2[i] = str1[i];
        str2[i] = 0;
}

void azzeraDato(Dato d[MAXN])
{
    int i;
    for (i = 0; i < MAXN; i++)
        d[i].durata = 0;
}

void stampa(Lista l)
{
    if (!l)
        printf("La lista è vuota.\n");
    while (l)
    {
        printf("%s %f\n", l->dato.codice, l->dato.durata);
        l = l->next;
    }
}

void nInsCoda(Lista *l, Dato *d, int n)
{
    int i;
    Lista *ls = l;
    for (i = 0; i < n; i++)
    {
        l = cercaCoda(l);
        insTesta(l, d[i]);
        l = ls;
    }
}

Lista *cercaCoda(Lista *l)
{
    while (*l)
        l = &(*l)->next;

    return l;
}

void insTesta(Lista *l, Dato d)
{
    Nodo *aux = (Nodo *)malloc(sizeof(Nodo));
    aux->dato = d;
    aux->next = *l;
    *l = aux;
}

int calcolaDato(char tempS[MAXL], float oraI, float oraF, int nM, Dato d[MAXN])
{
    int i;
    for (i = 0; i < nM; i++)
        if (confrontaStringhe(tempS, d[i].codice))
        {
            d[i].durata += oraF - oraI;
            break;
        }

    if (i == nM)
    {
        inserisciStringa(tempS, d[nM].codice, MAXL);
        d[nM].durata = oraF - oraI;
        nM++;
    }

    return nM;
}

void inserisciBinario(FILE *fB, Lista *l)
{
    Dato d[MAXN];
    Lista Ls;
    int i = 0, j;

    nuovaLista(&Ls);
    while (!feof(fB))
    {
        fread(d[i].codice, sizeof(char), MAXL, fB);
        fread(d[i].nome, sizeof(char), MAXL2, fB);
        fread(&d[i].anno, sizeof(int), 1, fB);
        if (!feof(fB))
            i++;
    }
    j = i;
    for (i = 0; i < j; i++)
    {
        Ls = *cerca(l, d[i]);
        if (Ls)
        {
            inserisciStringa(d[i].nome, Ls->dato.nome, MAXL2);
            Ls->dato.anno = d[i].anno;
        }
    }
}

Lista *cerca(Lista *l, Dato d)
{
    while (*l)
    {
        if (confrontaStringhe((*l)->dato.codice, d.codice))
            break;

        l = &(*l)->next;
    }
    return l;
}

void stampa2(Lista l)
{
    if (!l)
        printf("La lista è vuota.\n");
    while (l)
    {
        printf("Corso di: %s, anno: %d, Ore settimanali: %f\n", l->dato.nome, l->dato.anno, l->dato.durata);
        l = l->next;
    }
}