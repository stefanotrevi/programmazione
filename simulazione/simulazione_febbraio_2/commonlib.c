//info nell'header
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "commonlib.h"
#include "supportlib.h"

void RNG(int *out, int min, int max, int n)
{
    int range = max - min + 1, i;
    srand(time(NULL));
    for (i = 0; i < n; i++)
        out[i] = rand() % (range) + min;
}

void ordinaReplace(int *v, int dim)
{
    int i, j, temp;
    for (i = 0; i < dim; i++)
    {
        j = 0;
        while (j != i && v[i] < v[i - j - 1])
            j++;
        if (j)
        {
            temp = v[i - j];
            v[i - j] = v[i];
            v[i] = temp;
            i--;
        }
    }
}

void ordinaRicorsivo(int *mainV, int *v, int dim, int *fill)
{
    int pivot, *v1, *v2, ndim1, ndim2, average = dim / 2, flag = 1;

    if (checkArray(v, dim))
    {
        riempiMainArray(mainV, v, dim, fill);
    }
    else
    {
        do
        {
            ndim1 = 0;
            pivot = v[average];
            calcolaDim(v, dim, pivot, average, &ndim1, &ndim2);
            if (!ndim2 && average > 0 && flag)
                average--;
            else if (!ndim2)
            {
                if (average == 0)
                {
                    flag = 0;
                    average = dim / 2;
                }
                average++;
            }

        } while (!ndim2);
        v1 = (int *)malloc(ndim1 * sizeof(int));
        v2 = (int *)malloc(ndim2 * sizeof(int));
        riempiPartialArray(v, v1, v2, dim, ndim2, pivot, average);
        ordinaRicorsivo(mainV, v1, ndim1, fill);
        mainV[*fill] = pivot;
        *fill = *fill + 1;
        ordinaRicorsivo(mainV, v2, ndim2, fill);
        free(v1);
        free(v2);
    }
}

void printArray(int *v, int dim)
{
    int i;
    for (i = 0; i < dim; i++)
        printf("%d ", v[i]);
    printf("\n");
}

void fattorizzazione(int a)
{
    int b = 2, i, c, j, f;

    while (b <= sqrt(a))
    {
        i = 0;
        c = a;
        j = 2;
        f = 1;

        while (j < b)
        {
            if (b % j != 0)
            {
                j++;
                j += 2;
            }
            else
            {
                f = 0;
                break;
            }
        }

        while (!(c % b) && f)
        {
            c = c / b;
            i++;
        }

        if (i != 0)
        {
            printf("%d^%d", b, i);
            printf(" x ");
        }

        a = a / pow(b, i);
        b++;
    }
    if (a != 1)
        printf("%d^1\n", a);
}

int potenza(int base, int esp)
{

    int arg, err = 0;
    arg = base;
    if (esp == 0 && base == 0)
        err = 1;
    else if (esp == 0)
        arg = 1;
    for (int i = 1; i < esp; i++)
    {

        arg = arg * base;
    }
    if (err)
        return 0;
    else
        return arg;
}

int numeroContrario(int a)
{

    int b, inv = 0, i = calcolaCifre(a);
    do
    {
        i--;
        b = a % 10;
        a /= 10;
        inv += b*potenza(10, i);
    } while (a);

    return inv;
}

void nscanf(int *buff, int n)
{
    int i, in;
    printf("Inserire %d elementi:\n", n);
    for (i = 0; i < n; i++)
        scanf("%d", &buff[i]);
}

void eccezioneArg(int argc, int argHp)
{
    if (argc != argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE *fB, FILE *fT)
{

    if (fB == NULL || fT == NULL)
    {
        printf("Almeno uno dei file indicati non esiste.\n");
        exit(-2);
    }
}