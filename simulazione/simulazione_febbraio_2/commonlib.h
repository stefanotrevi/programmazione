/* in questa libreria sono contenute diverse funzioni create da me durante il corso di programmazione,
che si rivelano utili di volta in volta */

//genera "n" numeri casuali fra il minimo e il massimo, inclusi, inserendoli nell'array "out")
void RNG(int *out, int min, int max, int n);

//implementazione dell'InsertionSort su un vettore "v" di dimensione "dim"
void ordinaReplace(int *v, int dim);

//implementazione dell'algoritmo di Quicksort, applicabile ad un vettore di interi
// I parametri sono: vettore di destinazione, vettore di partenza (possono coincidere), dimensione del vettore, e 0 ("fill")
void ordinaRicorsivo(int *mainV, int *v, int dim, int *fill);

// stampa un array di interi di dimensione "dim"
void printArray(int *v, int dim);

// stampa a console la fattorizzazione di un numero
void fattorizzazione(int a);

// calcola la potenza di interi
int potenza(int base, int esp);

// inverte le cifre di un numero senza usare array
int numeroContrario(int a);

/*scansiona "n" elementi inserendoli nel vettore "buff". Potrebbe essere necessario
modificare la funzione a seconda di come si voglia effettuare la scansione, ad es.
se si conosce o meno a priori il numero di elementi da inserire*/
void nscanf(int *buff, int n);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int argc, int argHp);

//controlla che i file esistano
void eccezioneFile(FILE *fB, FILE *fT);