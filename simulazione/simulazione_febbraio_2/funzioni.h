//Questa librerie include le funzioni relative alle liste collegate

//la definizione di dato può essere ampliata come struct, ma alcune
//delle procedure/funzioni hanno bisogno di essere leggermente modificate

#define MAXL 10
#define MAXL2 31
#define MAXN 100
typedef struct
{
    char codice[MAXL];
    char nome[MAXL2];
    int anno;
    float durata;
} Dato;

typedef struct nodo
{
    Dato dato;
    struct nodo *next;
} Nodo;

typedef Nodo *Lista;

//inizializza una lista collegata
void nuovaLista(Lista *l);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int argc, int argHp);

//controlla che i file esistano
void eccezioneFile(FILE *fT, FILE *fB);

//inserisce i dati contenuti nel file di testo all'interno della lista
void inserisciFile(FILE *fT, Lista *l);

// 1 se uguali, 0 se diverse
int confrontaStringhe(char str1[MAXL], char str2[MAXL]);

//inserisce la stringa "str1" nella "str2"
void inserisciStringa(char *str1, char *str2, int max);

//azzera tutti gli elementi del campo indicato
void azzeraDato(Dato d[MAXN]);

//stampa una lista "l"
void stampa(Lista l);

//inserisce "n" dati appartenenti al vettore "d" in coda alla lista "l"
void nInsCoda(Lista *l, Dato *d, int n);

//modifica della funzione "cerca", per operare nella funzione "insCoda"
Lista *cercaCoda(Lista *l);

//inserisce il dato "n" in testa alla lista "l"
void insTesta(Lista *l, Dato n);

//inserisce i dati "grezzi" nella struttura dato
int calcolaDato(char tempS[MAXL], float oraI, float oraF, int nM, Dato d[MAXN]);

//aggiorna la lista con i dati del file binario
void inserisciBinario(FILE *fB, Lista *l);

//cerca il dato "d" nella lista "l", restituendo l'indirizzo del primo nodo
//che soddisfa la ricerca (es. se l = [1, 2, 3] e d = 2, return = [2, 3])
Lista *cerca(Lista *l, Dato d);

//altro tipo di stampa
void stampa2(Lista l);