#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

int main(int argc, char **argv)
{
    FILE *fT, *fB;
    Lista L;
    eccezioneArg(argc, 3);
    fT = fopen(argv[1], "rt");
    fB = fopen(argv[2], "rb");
    eccezioneFile(fT, fB);
    nuovaLista(&L);
    inserisciFile(fT, &L);
    stampa(L);
    inserisciBinario(fB, &L);
    stampa2(L);
    fclose(fT);
    return 0;
}