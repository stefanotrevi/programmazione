#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXD 31
#define MAXL 10

int main(void)
{
    FILE *fB = fopen("materie.dat", "wb");
    char codice[MAXL], nome[MAXD];
    int anno, i, j, k;

    if (fB == NULL)
        exit(-1);

    printf("Inserire numero materie:\n");
    scanf("%d", &i);
    j = i;
    printf("Inserire codice, nome e anno per ogni materia:\n");
    for (i; i > 0; i--)
    {
        printf("%d:\n", j - i + 1);
        scanf("%s", codice);
        fflush(stdin);
        for (k = 0; k < MAXD; k++)
        {
            char temp;
            scanf("%c", &temp);
            if (temp == '\n')
            {
                nome[k] = 0;
                break;
            }
            nome[k] = temp;
        }
        scanf("%d", &anno);
        fwrite(codice, sizeof(char), MAXL, fB);
        fwrite(nome, sizeof(char), MAXD, fB);
        fwrite(&anno, sizeof(int), 1, fB);
    }
    fclose(fB);
    return 0;
}