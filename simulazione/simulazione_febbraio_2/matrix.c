#include <stdio.h>

#define R 2
#define C 2

void f(float *x, float *y);
void prodotto(float M1[][C], float M2[][C], float M3[][C]);
void stampaMatrice(float M[][C]);

int main(void)
{
    float a = 1, b = 3, c = a / b;
    float MBB[R][C] = {{1, 1}, {-1, 1}}, MBIB[R][C] = {{1, 2}, {1, -1}}, MBBI[R][C] = {{c, 2 * c}, {c, -c}}, MBIBI[R][C];
    float PBI[R][C] = {{1, 0}, {0, 0}}, PB[R][C], QB[R][C], QBI[R][C];

    prodotto(MBB, MBIB, MBIBI);
    prodotto(MBBI, MBIBI, MBIBI);
    stampaMatrice(MBIBI);
    return 0;
}

void f(float *x, float *y)
{
    float temp = *x;
    *x = *x + *y;
    *y = *y - temp;
}

void prodotto(float M1[][C], float M2[][C], float M3[][C])
{
    int i, j, k;
    float MS[R][C] = {0};

    for (i = 0; i < R; i++)
        for (j = 0; j < C; j++)
            for (k = 0; k < C; k++)
                MS[i][j] += M1[i][k] * M2[k][j];
    for (i = 0; i < R * C; i++)
        M3[0][i] = MS[0][i];
}

void stampaMatrice(float M[][C])
{
    int i, j;

    for (i = 0; i < R; i++)
    {
        for (j = 0; j < C; j++)
            printf("%f ", M[i][j]);
        printf("\n");
    }
}