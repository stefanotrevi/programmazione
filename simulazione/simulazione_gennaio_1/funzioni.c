#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "funzioni.h"

void nuovaLista(Lista *l)
{
    *l = NULL;
}
int vuota(Lista l)
{
    return l == NULL;
}

int piena(Lista l)
{
    return 0;
}

void insTesta(Lista *l, Dato d)
{
    Nodo *aux = (Nodo *)malloc(sizeof(Nodo));
    aux->dato = d;
    aux->next = *l;
    *l = aux;
}

void nInsTesta(Lista *l, Dato *d, int n)
{
    int i;
    for (i = 0; i < n; i++)
    {
        Nodo *aux = (Nodo *)malloc(sizeof(Nodo));
        aux->dato = d[i];
        aux->next = *l;
        *l = aux;
    }
}

void insOrdinato(Lista *l, Dato d)
{
    l = cercaOrdinato(l, d);
    insTesta(l, d);
}

void nInsOrdinato(Lista *l, Dato *d, int n)
{
    int i;
    Lista *Ls = l;
    for (i = 0; i < n; i++)
    {
        l = cercaOrdinato(l, d[i]);
        insTesta(l, d[i]);
        l = Ls;
    }
}

void insCoda(Lista *l, Dato d)
{
    l = cercaCoda(l);
    insTesta(l, d);
}

void nInsCoda(Lista *l, Dato *d, int n)
{
    int i;
    Lista *ls = l;
    for (i = 0; i < n; i++)
    {
        l = cercaCoda(l);
        insTesta(l, d[i]);
        l = ls;
    }
}
void ordina(Lista *l)
{
    Lista Lo;
    nuovaLista(&Lo);
    while (*l)
    {
        insOrdinato(&Lo, (*l)->dato);
        *l = (*l)->next;
    }
    *l = Lo;
}

void eliminTesta(Lista *l)
{
    if (*l)
        *l = (*l)->next;
    else
        printf("E kome, Gokov, kome, puoi eliminave la mia tefta... Fe non ho una tefta? BUAHAHAH!\n");
}

int elimina(Lista *l, Dato d)
{
    l = cerca(l, d);
    if (*l)
    {
        eliminTesta(l);
        return 1;
    }
    else
        return 0;
}

int eliminaTutti(Lista *l, Dato d)
{
    int n = 0;
    while (*l)
    {
        l = cerca(l, d);
        eliminTesta(l);
        n++;
    }
    return n;
}

void stampa(Lista l)
{
    if (!l)
        printf("La lista è vuota.\n");
    while (l)
    {
        printf("%d ", l->dato);
        l = l->next;
    }
    printf("\n");
}

Lista reverse(Lista l)
{
    Lista Lr;
    nuovaLista(&Lr);
    while (l)
    {
        insTesta(&Lr, l->dato);
        l = l->next;
    }
    return Lr;
}

int lunghezza(Lista l)
{
    int i = 0;
    while (l)
    {
        i++;
        l = l->next;
    }
    return i;
}

Lista *cerca(Lista *l, Dato d)
{
    while (*l)
    {
        if (!strcmp((*l)->dato.nome, d.nome))
            break;

        l = &(*l)->next;
    }
    return l;
}

Lista *cercaOrdinato(Lista *l, Dato d)
{
    while (*l)
    {
        if (strcmp((*l)->dato.nome, d.nome) >= 0)
            break;

        l = &(*l)->next;
    }
    return l;
}

Lista *cercaCoda(Lista *l)
{
    while (*l)
        l = &(*l)->next;

    return l;
}

void eccezioneArg(int argc)
{
    if (argc != 3)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE *fB, FILE *fT)
{

    if (fB == NULL || fT == NULL)
    {
        printf("Almeno uno dei file indicati non esiste.\n");
        exit(-2);
    }
}

void FileInsCoda(Lista *l, FILE *fB)
{
    Dato temp[MAXN];
    int i;
    fread(temp, sizeof(Dato), MAXN, fB);
    for (i = 0; temp[i].calorie; i++)
        ;
    nInsCoda(l, temp, i);
}

float calcolaCal(Lista *l, FILE *fT)
{
    int i, peso;
    float calPasto = 0;
    Dato cibo[MAXN];
    for (i = 0; !feof(fT); i++)
    {
        fscanf(fT, "%s%d", &cibo[i].nome, &peso);
        if (!(*cerca(l, cibo[i])))
            printf("Spiacenti, \"%s\" non trovato nella tabella calorica a disposizione.\nControllare eventuali errori (es. lettere maiuscole)\n", cibo[i].nome);
        else
            calPasto += ((*cerca(l, cibo[i]))->dato.calorie * peso) / 100;
    }
    return calPasto;
}