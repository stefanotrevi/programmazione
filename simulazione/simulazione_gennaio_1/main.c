#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

int main(int argc, char **argv)
{
    FILE *fT, *fB;
    Lista Ctab;

    eccezioneArg(argc);
    fB = fopen(argv[1], "rb");
    fT = fopen(argv[2], "rt");
    eccezioneFile(fB, fT);
    nuovaLista(&Ctab);
    FileInsCoda(&Ctab, fB);
    printf("Totale calorie del pasto: %f\n", calcolaCal(&Ctab, fT));

    return 0;
}