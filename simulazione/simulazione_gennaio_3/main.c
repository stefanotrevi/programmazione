#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

int main(int argc, char **argv)
{
    int anno;
    FILE *fbIn = fopen("clienti.dat", "rb"), *fbOut = fopen("clientiRecenti.dat", "wb"), *fT = fopen("esclusi.txt", "wt");
    Lista L;
    
    eccezioneArg(argc, 2);
    sscanf(argv[1], "%d", &anno);
    eccezioneFile(fbIn, fbOut, fT);
    nuovaLista(&L);
    FileInsCoda(&L, fbIn);
    FileOut(L, fbOut, anno);
    stampaEsclusi(L, fT, anno);
    return 0;
}