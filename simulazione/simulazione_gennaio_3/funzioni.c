#include <stdio.h>
#include <stdlib.h>
#include "funzioni.h"

void nuovaLista(Lista *l)
{
    *l = NULL;
}

void eliminTesta(Lista *l)
{
    if (*l)
        *l = (*l)->next;
    else
        printf("Impossibile eliminare la testa, la lista è vuota!\n");
}

Lista *cercaCoda(Lista *l)
{
    while (*l)
        l = &(*l)->next;

    return l;
}

void insTesta(Lista *l, Dato d)
{
    Nodo *aux = (Nodo *)malloc(sizeof(Nodo));
    aux->dato = d;
    aux->next = *l;
    *l = aux;
}

void nInsCoda(Lista *l, Dato *d, int n)
{
    int i;
    Lista *ls = l;
    for (i = 0; i < n; i++)
    {
        l = cercaCoda(l);
        insTesta(l, d[i]);
        l = ls;
    }
}

void eccezioneArg(int argc, int argHp)
{
    if (argc != argHp)
    {
        printf("Argomenti di input incompleti.\n");
        exit(-1);
    }
}

void eccezioneFile(FILE *f1, FILE *f2, FILE *f3)
{

    if (f1 == NULL || f2 == NULL || f3 == NULL)
    {
        printf("Almeno uno dei file indicati non esiste.\n");
        exit(-2);
    }
}

void FileInsCoda(Lista *l, FILE *fB)
{
    Dato temp[MAXN];
    int i;

    //modificare il campo ".[qualcosa]" per aggiustare la funzione in base al tipo di dato
    for (i = 0; i < MAXN; i++)
        temp[i].anno = -1;
    fread(temp, sizeof(Dato), MAXN, fB);
    for (i = 0; temp[i].anno != -1; i++)
        ;
    nInsCoda(l, temp, i);
}

void FileOut(Lista l, FILE *fB, int anno)
{
    Dato temp;
    while (l)
    {
        temp = l->dato;
        if (temp.anno >= anno)
            fwrite(&temp, sizeof(Dato), 1, fB);
        eliminTesta(&l);
    }
}

void stampaEsclusi(Lista l, FILE *fT, int anno)
{
    while (l)
    {
        if(l->dato.anno < anno)
        fprintf(fT, "%s %d\n", l->dato.nome, l->dato.anno);
        eliminTesta(&l);
    }
}