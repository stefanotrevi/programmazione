//Questa libreria include le funzioni relative alle liste collegate
#define MAXL 31
#define MAXN 100

//la definizione di dato può essere ampliata come struct, ma alcune
//delle procedure/funzioni hanno bisogno di essere leggermente modificate
typedef struct
{
    char nome[MAXL];
    int anno;
} Dato;

typedef struct nodo
{
    Dato dato;
    struct nodo *next;
} Nodo;

typedef Nodo *Lista;

//inizializza una lista collegata
void nuovaLista(Lista *l);

//elimina la testa alla lista "l"
void eliminTesta(Lista *l);

//inserisce il dato "n" in testa alla lista "l"
void insTesta(Lista *l, Dato n);

//inserisce "n" dati appartenenti al vettore "d" in coda alla lista "l"
void nInsCoda(Lista *l, Dato *d, int n);

//modifica della funzione "cerca", per operare nella funzione "insCoda"
Lista *cercaCoda(Lista *l);

//controlla che gli input da riga di comando (argc) ci siano tutti (argHp)
void eccezioneArg(int argc, int argHp);

//controlla che i file esistano
void eccezioneFile(FILE *f1, FILE *f2, FILE *f3);

//inserisce i dati raccolti dal file "fB" nella lista "l", questa funzione deve essere
//modificata in modo appropriato in base ai dati che si intende raccogliere
void FileInsCoda(Lista *l, FILE *fB);

//esegue l'output di una lista in un file binario
void FileOut(Lista l, FILE *fB, int anno);

void stampaEsclusi(Lista l, FILE *fT, int anno);